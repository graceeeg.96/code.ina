package it.uniba.di.allaromanapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static android.content.ContentValues.TAG;

public class SettingsAccountFragment extends Fragment {
    private final int USERNAME_MAX_LENGHT = 20;
    private final int MAIL_MAX_LENGHT = 40;
    private final int USERNAME_MIN_LENGHT = 3;
    private final int MAIL_MIN_LENGHT = 5;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    FirebaseDatabase firebaseDatabase;

    CheckBox showHideCurrentPswCheckbox;
    EditText currentPassword, username, email;
    Button updateEmail, resetPasswordButton, updateUsername, deleteAccountButton;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_settings_account, container, false);


        updateEmail = view.findViewById(R.id.btn_update);
        resetPasswordButton = view.findViewById(R.id.btn_resetPassword);
        username = view.findViewById(R.id.username);
        username.setHint(currentUser.getDisplayName());
        deleteAccountButton = view.findViewById(R.id.btn_delete);

        email = view.findViewById(R.id.new_mail);
        email.setHint(currentUser.getEmail());
        progressBar=view.findViewById(R.id.updateDataProgressBar);

        showHideCurrentPswCheckbox = view.findViewById(R.id.showOldPasswordCheckbox);
        currentPassword = view.findViewById(R.id.current_Password);

        updateUsername = view.findViewById(R.id.btn_update_username);

        updateUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checkUsername = true;
                String usernameText = username.getText().toString().trim();

                // Username check
                if (!TextUtils.isEmpty((usernameText))){
                    if (usernameText.length() < USERNAME_MIN_LENGHT){
                        checkUsername = false;
                        username.setError(getText(R.string.usernameTooSmall));
                        username.requestFocus();
                    }

                    if (usernameText.length() > USERNAME_MAX_LENGHT){
                        checkUsername = false;
                        username.setError(getText(R.string.usernameTooLong));
                        username.requestFocus();
                    }
                } else {
                    checkUsername = false;
                }

                if (checkUsername) {
                    closeKeyboard();
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(usernameText).build();
                    currentUser.updateProfile(profileUpdates);
                    DatabaseReference dbRefUser = firebaseDatabase.getInstance().getReference(DBNodes.USERS).child(currentUser.getUid());
                    dbRefUser.setValue(usernameText).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                        }
                    });
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.username_updated, usernameText), Toast.LENGTH_SHORT).show();
                    username.getText().clear();
                    username.setHint(usernameText);
                }
            }
        });

        showHideCurrentPswCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if (value) {
                    //Show Password
                    currentPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    //Hide Password
                    currentPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                }
            }
        });

        deleteAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_delete)
                        .setTitle(R.string.removeAccountTitle)
                        .setMessage(R.string.removeAccountMsg)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                currentUser.delete();
                                logout();
                            }
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .show();
            }
        });

        resetPasswordButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                mAuth.sendPasswordResetEmail(currentUser.getEmail())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    progressBar.setVisibility(View.GONE);
                                    resetPasswordlogout();
                                    Log.d(TAG, "Reset password email sent.");
                                }
                            }
                        });
            }
        }));

        updateEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checkMail = true;
                String mailText = email.getText().toString().trim();
                updateEmail.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(mailText)) {
                    if (mailText.length() < MAIL_MIN_LENGHT) {
                        checkMail = false;
                        email.setError(getText(R.string.mailTooSmall));
                        email.requestFocus();
                    }

                    if (mailText.length() > MAIL_MAX_LENGHT) {
                        checkMail = false;
                        email.setError(getText(R.string.mailTooLong));
                        email.requestFocus();
                    }
                } else {
                    checkMail = false;
                }
                if (checkMail){
                    AuthCredential credential = EmailAuthProvider.getCredential(currentUser.getEmail(), currentPassword.getText().toString().trim());
                    currentUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    currentUser.updateEmail(email.getText().toString().trim()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        SendEmailVerificationMessage();
                                                    } else {
                                                        Toast.makeText(getActivity().getApplicationContext(), R.string.reset_password_failed, Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                }
                            });
                }


            }
        });

        return view;
    }

    private void closeKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void logout(){
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void resetPasswordlogout(){
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Reset Password", true);
        startActivity(intent);
    }

    private void SendEmailVerificationMessage(){
        currentUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    updateEmail.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity().getApplicationContext(), R.string.verification_message, Toast.LENGTH_SHORT ).show();
                }else{
                    String error=task.getException().getMessage();
                    Toast.makeText(getActivity().getApplicationContext(), R.string.LoginFailed+ error, Toast.LENGTH_SHORT ).show();
                }
            }
        });
        logout();
    }

}
