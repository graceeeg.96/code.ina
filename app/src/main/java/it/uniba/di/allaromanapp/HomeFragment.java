package it.uniba.di.allaromanapp;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseUser currentUser = mAuth.getCurrentUser();
    private Button btn_group, btn_join_existing_group;
    ListView group_list;
    ArrayList<String> groupKeys = new ArrayList<>();
    ArrayList<Group> groups = new ArrayList<>();
    ImageView pigNoGroup;
    ProgressBar searchingGroups;
    TextView yourGroupsLabel;
    ArrayList<Long> membersNum = new ArrayList<>();



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        // Group list
        databaseReference = firebaseDatabase.getInstance().getReference(DBNodes.GROUPS);
        group_list = view.findViewById(R.id.group_list);
        pigNoGroup = view.findViewById(R.id.pig_nogroup);
        pigNoGroup.setVisibility(View.GONE);
        searchingGroups = view.findViewById(R.id.searchingGroups);
        yourGroupsLabel = view.findViewById(R.id.yourGroupsLabel);
       //create new group
        TextView welcomeUser = view.findViewById(R.id.welcome_userLabel);
        welcomeUser.setText(getString(R.string.welcome_userLabel, currentUser.getDisplayName()));
        btn_group=view.findViewById(R.id.btn_new_group);
        btn_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), NewGroupActivity.class));
            }
        });
        //join an existing group
        btn_join_existing_group=view.findViewById(R.id.btn_join_existing_group);
        btn_join_existing_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), JoinNewGroupActivity.class));
            }
        });
        //select the group with its corresponding key
        group_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent toGroupIntent = new Intent(getContext(), GroupActivity.class);
                String groupKeySelected = groupKeys.get(position);
                toGroupIntent.putExtra("groupID", groupKeySelected);
                startActivity(toGroupIntent);
            }
        });

        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        // No groups view
        searchingGroups.setVisibility(View.VISIBLE);

        pigNoGroup.setVisibility(View.GONE);
        yourGroupsLabel.setText(R.string.noGroups);
        yourGroupsLabel.setGravity(Gravity.CENTER_HORIZONTAL);
        yourGroupsLabel.setVisibility(View.GONE);
        group_list.setVisibility(View.GONE);

        // Groups where user is Member or Admin
        final Query groupMember = databaseReference.orderByChild(DBNodes.MEMBERS + "/" + currentUser.getUid());
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                searchingGroups.setVisibility(View.INVISIBLE);
                groupKeys = new ArrayList<>();
                groups = new ArrayList<>();
                membersNum = new ArrayList<>();
                if (dataSnapshot.exists()){
                    for (final DataSnapshot groupSnap : dataSnapshot.getChildren()){
                        final Group newGroup = groupSnap.getValue(Group.class);
                        final DatabaseReference dbMembers = databaseReference.child(groupSnap.getKey()).child(DBNodes.MEMBERS);
                        dbMembers.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    for (DataSnapshot memberSnap : dataSnapshot.getChildren()){
                                        if (memberSnap.getKey().equals(currentUser.getUid())){
                                            groups.add(newGroup);
                                            groupKeys.add(groupSnap.getKey());
                                            Log.i("membri", String.valueOf(dataSnapshot.getChildrenCount()));
                                            membersNum.add(dataSnapshot.getChildrenCount());

                                        }

                                    }

                                    //the groups become visible
                                    if (groups.size()>0) {
                                        searchingGroups.setVisibility(View.INVISIBLE);
                                        yourGroupsLabel.setText(R.string.yourGroups);
                                        group_list.setVisibility(View.VISIBLE);
                                        pigNoGroup.setVisibility(View.GONE);
                                        yourGroupsLabel.setGravity(Gravity.LEFT);
                                        yourGroupsLabel.setVisibility(View.VISIBLE);

                                        GroupListAdapter groupListAdapter = new GroupListAdapter(getActivity(), groups, membersNum);
                                        group_list.setAdapter(groupListAdapter);

                                    } else {
                                        //no groups
                                        searchingGroups.setVisibility(View.INVISIBLE);
                                        yourGroupsLabel.setText(R.string.noGroups);
                                        group_list.setVisibility(View.INVISIBLE);
                                        pigNoGroup.setVisibility(View.VISIBLE);
                                        yourGroupsLabel.setGravity(Gravity.CENTER_HORIZONTAL);
                                        yourGroupsLabel.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    //no groups
                                    searchingGroups.setVisibility(View.INVISIBLE);
                                    yourGroupsLabel.setText(R.string.noGroups);
                                    group_list.setVisibility(View.INVISIBLE);
                                    pigNoGroup.setVisibility(View.VISIBLE);
                                    yourGroupsLabel.setGravity(Gravity.CENTER_HORIZONTAL);
                                    yourGroupsLabel.setVisibility(View.VISIBLE);
                                }



                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }

                        });

                    }






                } else {
                    //no groups
                        pigNoGroup.setVisibility(View.VISIBLE);
                        yourGroupsLabel.setVisibility(View.VISIBLE);
                        yourGroupsLabel.setText(R.string.noGroups);
                        yourGroupsLabel.setGravity(Gravity.CENTER_HORIZONTAL);
                        group_list.setVisibility(View.GONE);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });


    }


}

