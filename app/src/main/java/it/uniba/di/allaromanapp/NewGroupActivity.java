package it.uniba.di.allaromanapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class NewGroupActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener, Switch.OnCheckedChangeListener{

    private TextView defaultCategoryLabel, defaultCurrencyLabel;
    private EditText groupPassword, groupName;
    private Switch privateGroupSwitch;
    Button confirmGroupButton;
    ProgressBar creationGroupProgressBar;

    private final int NAME_MAX_LENGHT = 20;
    private final int NAME_MIN_LENGHT = 3;
    private final int PASSWORD_MAX_LENGHT = 8;
    private final int PASSWORD_MIN_LENGHT = 4;

    String currencyCode = "EUR", category;
    ImageView categoryArrow, currencyArrow;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);
        Toolbar toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        NewGroupActivity.this.setTitle(R.string.NewGroupLabel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        new AdaptiveDarkMode(NewGroupActivity.this);


        confirmGroupButton = findViewById(R.id.btn_create_new_group);
        defaultCategoryLabel = findViewById(R.id.defaultCategory);
        categoryArrow = findViewById(R.id.categoryMenuArrow);
        defaultCurrencyLabel = findViewById(R.id.defaultCurrency);
        currencyArrow = findViewById(R.id.currencyMenuArrow);
        groupPassword = findViewById(R.id.inputPasswordGroup);
        groupName = findViewById(R.id.inputGroupName);
        creationGroupProgressBar = findViewById(R.id.crationGroupProgressBar);
        privateGroupSwitch = findViewById(R.id.privateGroupSwitch);
        if (privateGroupSwitch != null) {
            privateGroupSwitch.setOnCheckedChangeListener(this);
        }

        defaultCurrencyLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCurrenciesPopupMenu(view);
            }
        });

        currencyArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCurrenciesPopupMenu(view);
            }
        });

        defaultCategoryLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCategoriesPopupMenu(view);
            }
        });
        categoryArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCategoriesPopupMenu(view);
            }
        });
        confirmGroupButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                confirmGroupButton.setVisibility(View.INVISIBLE);
                creationGroupProgressBar.setVisibility(View.VISIBLE);
                final int passwordLength = groupPassword.getText().length();
                final int groupNameLenght = groupName.getText().length();
                final String passwordText = groupPassword.getText().toString().trim();
                final String groupNameText = groupName.getText().toString().trim();
                boolean isPrivateAccess=privateGroupSwitch.isChecked();
                boolean checkOk = true;
                // Check on password
                if (privateGroupSwitch.isChecked()) {
                    if (passwordLength < PASSWORD_MIN_LENGHT || TextUtils.isEmpty(passwordText)) {
                        checkOk = false;
                        groupPassword.setError(getText(R.string.passwordTooSmall));
                        groupPassword.requestFocus();
                    }
                    if (passwordLength > PASSWORD_MAX_LENGHT) {
                        checkOk = false;
                        groupPassword.setError(getText(R.string.passwordTooLong));
                        groupPassword.requestFocus();
                    }
                }

                // Check on group name
                if (groupNameLenght < NAME_MIN_LENGHT || TextUtils.isEmpty(groupNameText)) {
                    checkOk = false;
                    groupName.setError(getText(R.string.groupNameTooSmall));
                    groupName.requestFocus();
                }
                if (groupNameLenght > NAME_MAX_LENGHT) {
                    checkOk = false;
                    groupName.setError(getText(R.string.groupNameTooLong));
                    groupName.requestFocus();
                }
                    if (checkOk) {
                        currencyCode = defaultCurrencyLabel.getText().toString();
                        category = defaultCategoryLabel.getText().toString();
                        Group newGroup = new Group(groupNameText, currencyCode, passwordText, isPrivateAccess, category);
                        createGroup(newGroup);

                    } else {
                        confirmGroupButton.setVisibility(View.VISIBLE);
                        creationGroupProgressBar.setVisibility(View.INVISIBLE);
                    }
                }
        });


    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if(isChecked) {
            groupPassword.setVisibility(View.VISIBLE);
        } else {
            groupPassword.setVisibility(View.INVISIBLE);
        }
    }

    public void showCurrenciesPopupMenu(View v) {
        PopupMenu popup=new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.popup_currencies_menu);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch(menuItem.getItemId()){
            // Currencies menu
            case R.id.EUR:
                defaultCurrencyLabel.setText("EUR");
                return true;
            case R.id.USD:
                defaultCurrencyLabel.setText("USD");
                return true;
            case R.id.JPY:
                defaultCurrencyLabel.setText("JPY");
                return true;
            case R.id.GBP:
                defaultCurrencyLabel.setText("GBP");
                return true;
            case R.id.CHF:
                defaultCurrencyLabel.setText("CHF");
                return true;
            case R.id.AUD:
                defaultCurrencyLabel.setText("AUD");
                return true;
            case R.id.other:
                startActivityForResult(new Intent(this, CurrenciesActivity.class), 2);
                return true;

            // Categories menu
            case R.id.travelLabel:
              defaultCategoryLabel.setText(R.string.DefaultCategoryLabel);
                 return true;
            case R.id.coupleLabel:
                defaultCategoryLabel.setText(R.string.CoupleLabel);
                return true;
                case R.id.sharedHouseLabel:
                    defaultCategoryLabel.setText(R.string.SharedHouseLabel);
                    return true;
            case R.id.EventLabel:
                defaultCategoryLabel.setText(R.string.EventLabel);
                return true;
            case R.id.ProjectLabel:
                defaultCategoryLabel.setText(R.string.ProjectLabel);
                return true;
            case R.id.otherLabel1:
                defaultCategoryLabel.setText(R.string.otherLabel);
                return true;
           default:
                return false;

        }
    }


    public void showCategoriesPopupMenu(View v){
        PopupMenu popup1=new PopupMenu(this, v);
        popup1.setOnMenuItemClickListener(this);
        popup1.inflate(R.menu.popup_categories_menu);
        popup1.show();
   }

   // "Other" currency option management
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent databack) {
        super.onActivityResult(requestCode, resultCode, databack);
        if (resultCode == Activity.RESULT_OK){
            currencyCode = databack.getStringExtra("currency");
            defaultCurrencyLabel.setText(currencyCode);
        }
    }

    public void createGroup(final Group group){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference dbRef = database.getReference(DBNodes.GROUPS).push();
        dbRef.setValue(group).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                String groupKeySelected = dbRef.getKey();

                // Initialize group member attributes: set "paid" to 0 on DB
                database.getReference(DBNodes.GROUPS).child(groupKeySelected).child(DBNodes.MEMBERS).child(currentUser.getUid()).child(DBNodes.PAID).setValue(0);
                final DatabaseReference dbMember = database.getReference(DBNodes.GROUPS).child(groupKeySelected).child(DBNodes.MEMBERS).child(currentUser.getUid()).child(DBNodes.ADMIN);
                // Set user to admin role
                dbMember.setValue(true).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(), R.string.createGroupSuccess, Toast.LENGTH_SHORT).show();
                        String groupKeySelected = dbRef.getKey();

                        // Add "creation group" log message to the group history
                        GroupLog currentLog = new GroupLog(getString(R.string.groupCreated, currentUser.getDisplayName(), group.getGroupName()));
                        database.getReference(DBNodes.GROUPS).child(groupKeySelected).child(DBNodes.LOGS).push().setValue(currentLog);

                        // User automatically accesses the group after creating it
                        Intent toGroupIntent = new Intent(getApplicationContext(), GroupActivity.class);
                        toGroupIntent.putExtra("groupID", groupKeySelected);
                        startActivity(toGroupIntent);
                        finish();
                    }
                });


            }
        });
    }

}
