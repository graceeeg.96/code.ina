package it.uniba.di.allaromanapp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

public class DebtListAdapter  extends ArrayAdapter<Debt> {

    private Context mContext;
    int mResource;
    String currencyGroupCode;

    public DebtListAdapter(Context context, int resource, ArrayList<Debt> objects, String currencyGroupCode){
        super(context, resource, objects);
        mContext=context;
        mResource=resource;
        this.currencyGroupCode = currencyGroupCode;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String debtor = getItem(position).getDebtorUsername();
        String creditor = getItem(position).getCreditorUsername();
        Float debtAmount = getItem(position).getDebtAmount();

        TextDrawable drawableD = TextDrawable.builder().buildRound(String.valueOf(debtor.charAt(0)).toUpperCase(), mContext.getResources().getColor(R.color.debtorColor));
        TextDrawable drawableC = TextDrawable.builder().buildRound(String.valueOf(creditor.charAt(0)).toUpperCase(), mContext.getResources().getColor(R.color.creditorColor));
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        ImageView debitorIcon = convertView.findViewById(R.id.debtor_icon);
        ImageView creditorIcon = convertView.findViewById(R.id.creditor_icon);
        debitorIcon.setImageDrawable(drawableD);
        creditorIcon.setImageDrawable(drawableC);

        TextView debtorLabel = convertView.findViewById(R.id.debtor);
        TextView creditorLabel = convertView.findViewById(R.id.creditor);
        TextView debtAmountLabel = convertView.findViewById(R.id.debt_amount);
        debtorLabel.setText(debtor);
        creditorLabel.setText(creditor);
        debtAmountLabel.setText(String.format("%.2f",debtAmount) + " " + currencyGroupCode);



        return convertView;
    }
}
