package it.uniba.di.allaromanapp;

public class Group {
    private String groupName;
    private String currencyCode;
    private String codeAccess;
    private String category;
    private boolean privateAccess;

    public Group(String groupName, String currencyCode, String codeAccess,boolean privateAccess, String category) {
        this.groupName = groupName;
        this.currencyCode = currencyCode;
        this.codeAccess = codeAccess;
        this.privateAccess=privateAccess;
        this.category = category;

    }

    public Group(){

    }

    public boolean isPrivateAccess() {
        return privateAccess;
    }

    public void setPrivateAccess(boolean privateAccess) {
        this.privateAccess = privateAccess;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getcodeAccess() {
        return codeAccess;
    }

    public void setcodeAccess(String codeAccess) {
        this.codeAccess = codeAccess;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
