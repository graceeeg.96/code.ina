package it.uniba.di.allaromanapp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;


public class BalancesAdapter extends ArrayAdapter<Balance> {


    private Context mContext;
    int mResource;
    String currencyGroupCode;


    public BalancesAdapter( Context context, int resource, ArrayList<Balance> objects, String currencyGroupCode) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        this.currencyGroupCode = currencyGroupCode;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView,  ViewGroup parent) {
       String usernameLabel = getItem(position).getUsername();
       Float amountBalance = getItem(position).getBalance();

       // Letter text group logo
        ColorGenerator generator = ColorGenerator.MATERIAL;
        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(usernameLabel.charAt(0)).toUpperCase(), generator.getColor(position));

        LayoutInflater inflater=LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        ImageView userIcon = convertView.findViewById(R.id.user_icon);
        TextView memberUsername = convertView.findViewById(R.id.usernameLabel);
        TextView balanceLabel = convertView.findViewById(R.id.balanceLabel);


        balanceLabel.setText(String.format("%.2f", amountBalance)+ " " + currencyGroupCode);
        // Change color based on the balance
        if(amountBalance<0){
            balanceLabel.setTextColor(mContext.getResources().getColor(R.color.debtorColor));
        }else{
            balanceLabel.setTextColor(mContext.getResources().getColor(R.color.creditorColor));
        }
        userIcon.setImageDrawable(drawable);
        memberUsername.setText(usernameLabel);
        return convertView;


    }
}
