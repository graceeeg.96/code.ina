package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.AuthResult;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class SignupActivity extends AppCompatActivity {
    FirebaseAuth mAuth;

    private final int USERNAME_MAX_LENGHT = 20;
    private final int MAIL_MAX_LENGHT = 40;
    private final int PASSWORD_MAX_LENGHT = 20;
    private final int USERNAME_MIN_LENGHT = 3;
    private final int MAIL_MIN_LENGHT = 5;
    private final int PASSWORD_MIN_LENGHT = 8;

    Button loginButton, signupButton;
    CheckBox checkbox;
    EditText password, username, mail;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mAuth = FirebaseAuth.getInstance();

        loginButton = findViewById(R.id.btn_login);
        checkbox = findViewById(R.id.showHidePswCheckbox);
        password = findViewById(R.id.Password);
        username = findViewById(R.id.username);
        mail = findViewById(R.id.mailAddress);
        signupButton = findViewById(R.id.btn_signup);
        progressBar = findViewById(R.id.progressBar);

        signupButton.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int passwordLength = password.getText().length();
                final int usernameLength = username.getText().length();
                final int mailLength = mail.getText().length();
                boolean checkOk = true;

                final String mailText = mail.getText().toString().trim();
                String passwordText = password.getText().toString().trim();
                final String usernameText = username.getText().toString().trim();

                // Check on password
                if (passwordLength < PASSWORD_MIN_LENGHT || TextUtils.isEmpty(passwordText)) {
                    checkOk = false;
                    password.setError(getText(R.string.passwordTooSmall));
                    password.requestFocus();
                }
                if (passwordLength > PASSWORD_MAX_LENGHT) {
                    checkOk = false;
                    password.setError(getText(R.string.passwordTooLong));
                    password.requestFocus();
                }

                // Check on username
                if (usernameLength < USERNAME_MIN_LENGHT || TextUtils.isEmpty(usernameText)) {
                    checkOk = false;
                    username.setError(getText(R.string.usernameTooSmall));
                    username.requestFocus();
                }
                if (usernameLength > USERNAME_MAX_LENGHT) {
                    checkOk = false;
                    username.setError(getText(R.string.usernameTooLong));
                    username.requestFocus();
                }
                // Check on mail
                if (mailLength < MAIL_MIN_LENGHT || TextUtils.isEmpty(mailText)) {
                    checkOk = false;
                    mail.setError(getText(R.string.mailTooSmall));
                    mail.requestFocus();
                }
                if (mailLength > MAIL_MAX_LENGHT) {
                    checkOk = false;
                    mail.setError(getText(R.string.mailTooLong));
                    mail.requestFocus();
                }

                if (checkOk) {
                    progressBar.setVisibility(View.VISIBLE);
                    // Try to register new user to Firebase
                    mAuth.createUserWithEmailAndPassword(mailText, passwordText).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                updateUsername(usernameText);
                                SendEmailVerificationMessage();
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.signupFailed, Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });

                } else {
                    Toast.makeText(getApplicationContext(), R.string.signupFailed, Toast.LENGTH_SHORT).show();
                }
            }
        });


        // Go to MainActivity
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(loginIntent);
                finish();
            }
        });

        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if (value) {
                    //Show Password
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    //Hide Password
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());

                }
            }
        });

    }

    // Update user account name with username given in registration field
    private void updateUsername(String username){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(username).build();
        user.updateProfile(profileUpdates);
    }

    private void SendEmailVerificationMessage(){
        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null){
            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(getApplicationContext(), R.string.verification_message, Toast.LENGTH_LONG ).show();
                    }else{
                        String error=task.getException().getMessage();
                        Toast.makeText(getApplicationContext(), R.string.LoginFailed+ error, Toast.LENGTH_SHORT ).show();
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

}







