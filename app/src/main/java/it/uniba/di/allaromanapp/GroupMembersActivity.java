package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GroupMembersActivity extends AppCompatActivity {
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    String groupID;
    ArrayList<Member> members = new ArrayList<>();
    ArrayList<String> memberKey = new ArrayList<>();
    GroupMembersAdapter membersListAdapter;
    ListView members_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_members);

        Toolbar toolbar = findViewById(R.id.group_members_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        new AdaptiveDarkMode(GroupMembersActivity.this);

        Intent intent = getIntent();
        groupID = intent.getStringExtra("groupID");
        databaseReference = firebaseDatabase.getInstance().getReference(DBNodes.GROUPS).child(groupID);
        members_list=findViewById(R.id.members_list);

   //kick a member out of the group (action accessible only to admin)
        members_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {


                DatabaseReference dbRef = databaseReference.child(DBNodes.MEMBERS + "/" + currentUser.getUid());
                dbRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child(DBNodes.ADMIN).getValue(Boolean.class)){
                            final DatabaseReference dbMembersRef = databaseReference.child(DBNodes.MEMBERS);
                            if (!members.get(position).getAdmin()) {
                                new AlertDialog.Builder(GroupMembersActivity.this)
                                        .setIcon(android.R.drawable.ic_delete)
                                        .setTitle(R.string.deleteMemberTitle)
                                        .setMessage(R.string.deleteMemberMessage)
                                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                membersListAdapter.notifyDataSetChanged();
                                                String notificationMsg = getString(R.string.memberRemoved, members.get(position).getUsername());
                                                startNotification(groupID, getString(R.string.memberRemovedTitle), notificationMsg);
                                                databaseReference.child(DBNodes.LOGS).push().setValue(new GroupLog(notificationMsg));
                                                dbMembersRef.child(memberKey.get(position)).removeValue();
                                                members.remove(position);
                                                memberKey.remove(position);

                                            }
                                        })
                                        .setNegativeButton(R.string.cancel, null)
                                        .show();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();


        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getSupportActionBar().setTitle(dataSnapshot.getValue(Group.class).getGroupName());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        // Search members ID and admin role
        final DatabaseReference dbMembersRef = databaseReference.child(DBNodes.MEMBERS);
        dbMembersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                members = new ArrayList<>();
                memberKey=new ArrayList<>();
                if (dataSnapshot.exists()) {
                    for (final DataSnapshot memberSnap : dataSnapshot.getChildren()) {
                        DatabaseReference dbRefUsers = firebaseDatabase.getInstance().getReference(DBNodes.USERS).child(memberSnap.getKey());
                        Log.i("membro", memberSnap.getKey());
                        memberKey.add(memberSnap.getKey());
                        // Search members username
                        dbRefUsers.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    members.add(new Member (dataSnapshot.getValue(String.class),memberSnap.child(DBNodes.ADMIN).getValue(Boolean.class)));
                                    membersListAdapter = new GroupMembersAdapter(getApplicationContext(), R.layout.item_member, members);
                                    members_list.setAdapter(membersListAdapter);
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }


                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void startNotification(String topic, String title, String message){
        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();
        try {
            notifcationBody.put("title", title);
            notifcationBody.put("message", message);
            notifcationBody.put("group", topic);
            notification.put("to", "/topics/" + topic);
            notification.put("data", notifcationBody);
        } catch (JSONException e) {
            Log.e("notification", e.getMessage() );
        }
        sendNotification(notification);

    }

    private void sendNotification(JSONObject notification) {
        final String FCM_API = "https://fcm.googleapis.com/fcm/send";
        final String serverKey = "key=" + "AAAAzshXOZs:APA91bE0xhqeTzDxEJr_cGu7tYRB8QU1jFl4ChFaZWfUWi5jYo4MFz2yvmw0pGF8jsYHZoee2HSx2Q20Pb4rnABc94ieGvVNRWlNVHX-m7-hZIJUng48kpxU4i8HchALEJ6z8tITV2fK";
        final String contentType = "application/json";
        final String TAG = "NOTIFICATION TAG";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(FCM_API, notification,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "onResponse: " + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(TAG, "onErrorResponse: Didn't work");
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", serverKey);
                params.put("Content-Type", contentType);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }



}
