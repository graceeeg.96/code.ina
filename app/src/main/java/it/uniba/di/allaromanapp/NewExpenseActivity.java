package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import org.json.JSONException;
import org.json.JSONObject;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class NewExpenseActivity extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    TextView codeCurrency, payedBy;
    String groupID;
    ImageView usernameMenuArrow;
    Button confirmExpense;
    EditText expenseTitle, expenseAmount;
    private final int TITLE_MAX_LENGHT = 30;
    private final int TITLE_MIN_LENGHT = 3;
    private final int AMOUNT_MAX_LENGHT = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        groupID = intent.getStringExtra("groupID");
        databaseReference = firebaseDatabase.getInstance().getReference(DBNodes.GROUPS).child(groupID);
        setContentView(R.layout.activity_new_expense);
        Toolbar toolbar = findViewById(R.id.toolbar8);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.new_expense_label_fab);
        final EditText Date, Time;
        Date = findViewById(R.id.inputDatePicker);
        Time = findViewById(R.id.inputTimePicker);
        codeCurrency = findViewById(R.id.codeCurrencyLabel);
        confirmExpense = findViewById(R.id.btn_create_new_expense);
        expenseTitle = findViewById(R.id.inputExpenseTitle);
        expenseAmount = findViewById(R.id.inputExpenseAmount);
        usernameMenuArrow = findViewById(R.id.UsernameMenuArrow);
        expenseAmount.setFilters(new InputFilter[] { filter });
        expenseTitle.setFilters(new InputFilter[] { filterTitle });

        new AdaptiveDarkMode(NewExpenseActivity.this);


        // 'Payed by' popup menu
        payedBy = findViewById(R.id.creditorNameLabel);
        payedBy.setText(currentUser.getDisplayName());
        final HashMap<String, String> payedByUsers = new HashMap<>();
        final String[] payedByID = {currentUser.getUid()};
        Context wrapper = new ContextThemeWrapper(this, R.style.PopupMenu);
        final PopupMenu popup = new PopupMenu(wrapper, payedBy);
        DatabaseReference dbRefMembers = databaseReference.child(DBNodes.MEMBERS);
        dbRefMembers.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot memberSnap : dataSnapshot.getChildren()) {
                    DatabaseReference dbRefAllUsers = firebaseDatabase.getInstance().getReference(DBNodes.USERS).child(memberSnap.getKey());
                    dbRefAllUsers.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            payedByUsers.put(dataSnapshot.getValue(String.class), memberSnap.getKey());
                            // Add username voice to "payed by" menu
                            popup.getMenu().add(dataSnapshot.getValue(String.class));
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        popup.getMenuInflater().inflate(R.menu.popup_username_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                payedBy.setText(item.getTitle());
                payedByID[0] = payedByUsers.get(item.getTitle());
                return true;
            }
        });

        usernameMenuArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.show();
            }
        });
        // End of 'Payed by' popup menu



        final Calendar[] calendar = {Calendar.getInstance()};
        final int year = calendar[0].get(Calendar.YEAR);
        final int month = calendar[0].get(Calendar.MONTH);
        final int day = calendar[0].get(Calendar.DAY_OF_MONTH);
        final int currentHour = calendar[0].get(Calendar.HOUR_OF_DAY);
        final int currentMinute = calendar[0].get(Calendar.MINUTE);

        String datemFormat = "MM";
        SimpleDateFormat sdf = new SimpleDateFormat(datemFormat);
        String monthFormat = sdf.format(month);


        Date.setHint(new StringBuilder().append(day).append("/").append(monthFormat).append("/").append(year));
            Time.setHint(String.format("%02d:%02d", currentHour, currentMinute));
            expenseAmount.setHint("00.00");

        Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(NewExpenseActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month = month + 1;
                        String monthString = String.valueOf(month);
                        if (monthString.length() == 1) {
                            monthString = "0" + monthString;
                        }
                        String dayString=String.valueOf(day);
                        if(dayString.length()==1){
                            dayString="0"+dayString;
                        }
                        String date = dayString + "/" + monthString + "/" + year;
                        Date.setText(date);

                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        Time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(NewExpenseActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                        Time.setText(String.format("%02d:%02d", hourOfDay, minutes));
                    }
                }, currentHour, currentMinute, true);

                timePickerDialog.show();
            }
        });


        Time.setInputType(InputType.TYPE_CLASS_TEXT);
        Time.requestFocus();
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.showSoftInput(Time, InputMethodManager.SHOW_FORCED);

        Date.setInputType(InputType.TYPE_CLASS_TEXT);
        Date.requestFocus();
        InputMethodManager mgr1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr1.showSoftInput(Date, InputMethodManager.SHOW_FORCED);

        confirmExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int expenseTitleLenght = expenseTitle.getText().length();
                final int expenseAmountLenght = expenseAmount.getText().length();
                final String expenseTitleText = expenseTitle.getText().toString().trim();
                final String expenseAmountNumber = expenseAmount.getText().toString();
                final String DateText = Date.getText().toString().trim();
                final String TimeText = Time.getText().toString().trim();
                float amount = (float) 00.00 ;

                boolean checkOk = true;

                // Title format check
                if (expenseTitleLenght < TITLE_MIN_LENGHT || TextUtils.isEmpty(expenseTitleText)) {
                    checkOk = false;
                    expenseTitle.setError(getText(R.string.expenseTitleTooSmall));
                    expenseTitle.requestFocus();
                }
                if (expenseTitleLenght > TITLE_MAX_LENGHT) {
                    checkOk = false;
                    expenseTitle.setError(getText(R.string.ExpenseTitleTooLong));
                    expenseTitle.requestFocus();
                }
                // Amount format check
                if (expenseAmountNumber.equals("")) {
                    checkOk = false;
                    expenseAmount.setError(getText(R.string.amountEmpty));
                } else {
                    amount = Float.parseFloat(expenseAmount.getText().toString());
                }
                if (expenseAmountLenght > AMOUNT_MAX_LENGHT) {
                    checkOk = false;
                    expenseAmount.setError(getText(R.string.amountTooLong));
                    expenseAmount.requestFocus();
                    String.format("{0:0.00}", expenseAmount);
                }

                // Date format check
                if (DateText.equals("")) {
                    String dateFormat = "MM";
                    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                    String monthFormat = sdf.format(month);
                    Date.setText(new StringBuilder().append(day).append("/").append(monthFormat).append("/").append(year));
                }
                // Time format check
                if (TimeText.equals("")) {
                    java.util.Date date = Calendar.getInstance().getTime();
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:MM");
                    String output = sdf.format(date);
                    Time.setText(output);


                }
                // If all ok
                if (checkOk) {
                        Log.i("utente", payedByID[0]);
                        Expense newExpense = new Expense(expenseTitleText, amount, payedByID[0], Date.getText().toString(), Time.getText().toString());
                        createExpense(newExpense);
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Group thisGroup = dataSnapshot.getValue(Group.class);
                codeCurrency.setText(thisGroup.getCurrencyCode());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void createExpense(final Expense expense) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference dbRef = database.getReference(DBNodes.GROUPS).child(groupID).child(DBNodes.EXPENSES).push();
        dbRef.setValue(expense).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), R.string.expense_created_successful, Toast.LENGTH_SHORT).show();
                String notificationMsg = getString(R.string.newExpenseMsg, payedBy.getText(), expense.getTitle(), String.valueOf(expense.getAmount()), codeCurrency.getText());
                database.getReference(DBNodes.GROUPS).child(groupID).child(DBNodes.LOGS).push().setValue(new GroupLog(notificationMsg));
                startNotification(groupID, getString(R.string.newExpenseTitle), notificationMsg);
                finish();

            }

        });

    }


    private void startNotification(String topic, String title, String message) {
        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();
        try {
            notifcationBody.put("title", title);
            notifcationBody.put("message", message);
            notifcationBody.put("group", topic);
            notification.put("to", "/topics/" + topic);
            notification.put("data", notifcationBody);
        } catch (JSONException e) {
            Log.e("notification", e.getMessage());
        }
        sendNotification(notification);

    }

    private void sendNotification(JSONObject notification) {
        final String FCM_API = "https://fcm.googleapis.com/fcm/send";
        final String serverKey = "key=" + "AAAAzshXOZs:APA91bE0xhqeTzDxEJr_cGu7tYRB8QU1jFl4ChFaZWfUWi5jYo4MFz2yvmw0pGF8jsYHZoee2HSx2Q20Pb4rnABc94ieGvVNRWlNVHX-m7-hZIJUng48kpxU4i8HchALEJ6z8tITV2fK";
        final String contentType = "application/json";
        final String TAG = "NOTIFICATION TAG";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(FCM_API, notification,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "onResponse: " + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(TAG, "onErrorResponse: Didn't work");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", serverKey);
                params.put("Content-Type", contentType);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }



    // To prevent bad amount format
    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            String blockCharacterSet = "-,";
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    // To prevent bad title format
    private InputFilter filterTitle = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            String blockCharacterSetTitle = "~#@-+/?:;'_=^|$%&*!";
            if (source != null && blockCharacterSetTitle.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

}