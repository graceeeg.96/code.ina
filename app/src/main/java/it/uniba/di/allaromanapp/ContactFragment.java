package it.uniba.di.allaromanapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.net.Uri;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ContactFragment extends Fragment {
    //developer e-mail
    private String[] email={"g.grasso16@studenti.uniba.it", "a.rutigliano39@studenti.uniba.it"};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_contacts, container, false);
         Button btn_contact = view.findViewById(R.id.btn_contact);
             btn_contact.setOnClickListener(new View.OnClickListener(){
                 @Override
                 public void onClick(View view) {
                     sendEmail();
                 }
             });

     return view;
    }
  //sending e-mail through implicit intent
public void sendEmail(){
    Intent intent=new Intent(Intent.ACTION_SEND);
    intent.setDataAndType(Uri.parse("mailto:"), "message/rfc822");
    intent.putExtra(Intent.EXTRA_EMAIL, email);
    startActivity(intent.createChooser(intent, getString(R.string.choose_email)));
}




}
