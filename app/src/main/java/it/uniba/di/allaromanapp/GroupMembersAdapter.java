package it.uniba.di.allaromanapp;

import android.content.Context;
import android.content.Intent;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;


import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class GroupMembersAdapter extends ArrayAdapter<Member> {

    private Context mContext;
    int mResource;

    public GroupMembersAdapter(Context context, int resource, ArrayList<Member> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {
        String username = getItem(position).getUsername();
        boolean admin = getItem(position).getAdmin();
        // Letter text group logo
        ColorGenerator generator = ColorGenerator.MATERIAL;
        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(username.charAt(0)).toUpperCase(), generator.getColor(position));

        LayoutInflater inflater=LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        ImageView groupIcon = convertView.findViewById(R.id.member_icon);
        TextView memberUsername = convertView.findViewById(R.id.member_name);
        TextView adminLabel = convertView.findViewById(R.id.adminLabel);
        groupIcon.setImageDrawable(drawable);
        memberUsername.setText(username);
        if (admin) {
            adminLabel.setText(R.string.admin);
        } else {
            adminLabel.setText("");
        }

        return convertView;

    }
}
