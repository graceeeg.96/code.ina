package it.uniba.di.allaromanapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class CurrenciesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currencies);

        ListView listView=findViewById(R.id.ListView);
        Toolbar toolbar = findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);
        CurrenciesActivity.this.setTitle(R.string.selectCurrencyLabel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //create currency objects
        Currency albanianLek=new Currency("ALL", getString(R.string.Albanianlek));
        Currency ArmenianDram=new Currency("AMD", getString(R.string.Armeniandram));
        Currency NetherlandsAntilleanGuilder=new Currency("AND", getString(R.string.NetherlandsAntilleanGuilder));
        Currency ArgentinePeso=new Currency("ARS", getString(R.string.Argentinepeso));
        Currency AustralianDollar=new Currency("AUD", getString(R.string.AustralianDollar));
        Currency CanadianDollar=new Currency("CAD", getString(R.string.CanadianDollar));
        Currency SwissFranc=new Currency("CHF", getString(R.string.SwissFranc));
        Currency CzeckCrown=new Currency("CZK", getString(R.string.CzechCrown));
        Currency euro=new Currency("EUR",getString(R.string.Euro));
        Currency PoundSterling=new Currency("GBP", getString(R.string.PoundSterling));
        Currency GibraltarPound=new Currency("GIP", getString(R.string.GibraltarPound));
        Currency FrancOfGuinea=new Currency("GNF", getString(R.string.FrancOfGuinea));
        Currency HongKongDollar=new Currency("HKD", getString(R.string.HongKongDollar));
        Currency HungerianForint=new Currency("HUF", getString(R.string.HungarianForint));
        Currency JapaneseYen=new Currency("JPY", getString(R.string.JapaneseYen));
        Currency KoreanWon=new Currency("KPW", getString(R.string.KoreanWon));
        Currency MexicanPeso=new Currency("MXN", getString(R.string.MexicanPeso));
        Currency NorwegianKrone=new Currency("NOK", getString(R.string.NorwegianKrone));
        Currency SwedishKrona=new Currency("SEK", getString(R.string.SwedishKrona));
        Currency TurkishLira=new Currency("TRY", getString(R.string.TurkishLira));
        Currency AmericanDollar=new Currency("USD", getString(R.string.AmericanDollar));
        Currency UruguayanPeso=new Currency("UYU", getString(R.string.UruguayanPeso));

        ArrayList<Currency> currency=new ArrayList<>();
        currency.add(albanianLek);
        currency.add(ArmenianDram);
        currency.add(NetherlandsAntilleanGuilder);
        currency.add(ArgentinePeso);
        currency.add(AustralianDollar);
        currency.add(CanadianDollar);
        currency.add(SwissFranc);
        currency.add(CzeckCrown);
        currency.add(euro);
        currency.add(PoundSterling);
        currency.add(GibraltarPound);
        currency.add(FrancOfGuinea);
        currency.add(HongKongDollar);
        currency.add(HungerianForint);
        currency.add(JapaneseYen);
        currency.add(KoreanWon);
        currency.add(MexicanPeso);
        currency.add(NorwegianKrone);
        currency.add(SwedishKrona);
        currency.add(TurkishLira);
        currency.add(AmericanDollar);
        currency.add(UruguayanPeso);


        final CurrencyListAdapter adapter= new CurrencyListAdapter(this, R.layout.adapter_view_layout, currency);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Currency currencySelected = (Currency) adapterView.getItemAtPosition(position);
                Intent intent = new Intent();
                intent.putExtra("currency", currencySelected.getCode());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });


    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED,intent);
        finish();

    }
}
