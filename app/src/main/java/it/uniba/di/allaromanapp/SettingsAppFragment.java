package it.uniba.di.allaromanapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

public class SettingsAppFragment extends Fragment  {

    Switch switchDarkModeManual, switchAdaptiveDarkMode, switchNotifications;
    String DARK_MODE = "darkMode";
    String ADAPTIVE_DARK_MODE = "adaptiveDarkMode";
    String NOTIFICATIONS = "notifications";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_settings_app, container, false);
        switchDarkModeManual = view.findViewById(R.id.switch3);
        switchAdaptiveDarkMode = view.findViewById(R.id.switch4);
        switchNotifications = view.findViewById(R.id.switch2);

        SharedPreferences pref = getContext().getSharedPreferences("AllaRomanAppSettings", 0);
        final SharedPreferences.Editor prefEditor = pref.edit();
        final boolean darkMode = pref.getBoolean(DARK_MODE, false);
        final boolean adaptiveDarkMode = pref.getBoolean(ADAPTIVE_DARK_MODE, true);
        final boolean notifications = pref.getBoolean(NOTIFICATIONS, true);
        switchDarkModeManual.setChecked(darkMode);
        switchAdaptiveDarkMode.setChecked(adaptiveDarkMode);
        switchNotifications.setChecked(notifications);

         // switch manual dark mode
        switchDarkModeManual.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.i("night", String.valueOf(AppCompatDelegate.getDefaultNightMode()));
                if (switchDarkModeManual.isChecked()){
                    prefEditor.putBoolean(DARK_MODE, true);
                    prefEditor.putBoolean(ADAPTIVE_DARK_MODE, false);
                    prefEditor.apply();
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                } else {
                    prefEditor.putBoolean(DARK_MODE, false);
                    prefEditor.apply();
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }

            }
        });

        if (switchDarkModeManual.isChecked()){
            switchAdaptiveDarkMode.setChecked(false);
            switchAdaptiveDarkMode.setEnabled(false);
            prefEditor.putBoolean(ADAPTIVE_DARK_MODE, false);
            prefEditor.apply();
        }
           //switch adaptive dark mode (semi-automatic) through light sensor
        switchAdaptiveDarkMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (switchAdaptiveDarkMode.isChecked()){
                    prefEditor.putBoolean(ADAPTIVE_DARK_MODE, true);
                    prefEditor.apply();
                } else {
                    prefEditor.putBoolean(ADAPTIVE_DARK_MODE, false);
                    prefEditor.apply();
                }
            }
        });

        //enable or disable notifications
        switchNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (switchNotifications.isChecked()){
                    prefEditor.putBoolean(NOTIFICATIONS, true);
                    prefEditor.apply();
                } else {
                    prefEditor.putBoolean(NOTIFICATIONS, false);
                    prefEditor.apply();
                }
            }
        });


        return view;
    }








}
