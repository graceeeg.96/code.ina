package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DebtActivity extends AppCompatActivity {

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    BalancesAdapter balancesAdapter;
    DebtListAdapter debtsAdapter;
    ArrayList<User> members = new ArrayList<>();
    ArrayList<String> memberKey = new ArrayList<>();
    ArrayList<Balance> balances = new ArrayList<>(); // Temp arraylist used to calculate real balance
    ArrayList<Balance> balancesPN = new ArrayList<>();
    ArrayList<Balance> creditors = new ArrayList<>(); // For positive balancesPN
    ArrayList<Balance> debtors = new ArrayList<>(); // For negative balancesPN
    ArrayList<Debt> debts = new ArrayList<>();
    TextView balanceLabel, giveorReceiveLabel, totalDebt, debtsLabel, currentBalancesLabel;
    ListView balances_list, debts_list;
    Uri uri;
    String groupID, LOG_TAG = "SEGUO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debt);
        Toolbar toolbar = findViewById(R.id.toolbar11);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        groupID = intent.getStringExtra("groupID");
        uri = getIntent().getData();
        if (uri != null){
            List<String> params = uri.getPathSegments();
            String groupIDFromLink = params.get(params.size()-1);
            Log.i("gruppo", groupIDFromLink);
            groupID = groupIDFromLink;
        }
        balanceLabel=findViewById(R.id.balanceLabel);
        giveorReceiveLabel =findViewById(R.id.giveOrReceiveLabel);
        totalDebt =findViewById(R.id.totalDebt);
        debtsLabel =findViewById(R.id.debtsLabel);
        currentBalancesLabel =findViewById(R.id.currentBalancesLabel);
        balances_list=findViewById(R.id.balances_list);
        debts_list = findViewById(R.id.debts_list);

        new AdaptiveDarkMode(DebtActivity.this);

        databaseReference = firebaseDatabase.getInstance().getReference(DBNodes.GROUPS).child(groupID);

        debts_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                final Debt debtSelected = (Debt) adapterView.getItemAtPosition(position);
                final String currencyCode = totalDebt.getText().toString().substring(totalDebt.getText().length() - 3);
                Log.i("debtselected", debtSelected.getCreditorUsername());

                final AlertDialog.Builder debtDialog = new AlertDialog.Builder(DebtActivity.this);
                debtDialog.setIcon(R.drawable.ic_done_all);
                debtDialog.setTitle(R.string.debtTitle);
                debtDialog.setMessage(getString(R.string.debtMsg, debtSelected.getDebtorUsername(), debtSelected.getDebtAmount(), currencyCode, debtSelected.getCreditorUsername()));
                // Debt payment function
                debtDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        // Update DB creditor balance (paid)
                        for (int i = 0; i < members.size(); i++) {
                            if (members.get(i).getUsername().equals(debtSelected.getCreditorUsername())) {
                                final DatabaseReference dbMembersRef = databaseReference.child(DBNodes.MEMBERS).child(members.get(i).getId()).child(DBNodes.PAID);
                                final ValueEventListener getOldPaid = new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        float oldValue = dataSnapshot.getValue(Float.class);
                                        dbMembersRef.setValue(oldValue + (-debtSelected.getDebtAmount()));
                                        dbMembersRef.removeEventListener(this);
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                };
                                dbMembersRef.addValueEventListener(getOldPaid);
                            }
                            // Update DB debtors balance (paid)
                            if (members.get(i).getUsername().equals(debtSelected.getDebtorUsername())) {
                                final DatabaseReference dbMembersRef = databaseReference.child(DBNodes.MEMBERS).child(members.get(i).getId()).child(DBNodes.PAID);
                                final ValueEventListener getOldPaid = new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        float oldValue = dataSnapshot.getValue(Float.class);
                                        dbMembersRef.setValue(oldValue + debtSelected.getDebtAmount());
                                        dbMembersRef.removeEventListener(this);
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                };
                                dbMembersRef.addValueEventListener(getOldPaid);
                            }

                        }
                        // Send "debt paid" notification to all group members
                        String notificationMsg = getString(R.string.debtMsgNotification, debtSelected.getDebtorUsername(), debtSelected.getDebtAmount(), currencyCode, debtSelected.getCreditorUsername());
                        startNotification(groupID, getString(R.string.debtTitleNotification), notificationMsg);
                        databaseReference.child(DBNodes.LOGS).push().setValue(new GroupLog(notificationMsg));
                        Toast.makeText(getApplicationContext(), R.string.debtTitleNotification, Toast.LENGTH_SHORT).show();
                        onBackPressed();

                    }
                });

                // Warn button function
                for(int j=0; j<members.size();j++) {
                    if(members.get(j).getUsername().equals(debtSelected.getCreditorUsername())) {
                        String creditorKey = members.get(j).getId();
                        if (currentUser.getUid().equals(creditorKey)) {
                            // Prepare for share link function
                            debtDialog.setNeutralButton(R.string.warnsButton, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent toGroupIntent = new Intent(Intent.ACTION_SEND);
                                    toGroupIntent.setType("text/plain");
                                    toGroupIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.debtOutStandingMsg, debtSelected.getDebtorUsername(), debtSelected.getDebtAmount(), currencyCode, getSupportActionBar().getTitle(), groupID));
                                    toGroupIntent.putExtra("groupID", groupID);
                                    startActivity(Intent.createChooser(toGroupIntent, getString(R.string.share_app_via)));
                                }
                            });
                        }
                    }
                }

                debtDialog.setNegativeButton(R.string.notYet, null);
                debtDialog.show();
            }


        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (uri != null){
            List<String> params = uri.getPathSegments();
            String groupIDFromLink = params.get(params.size()-1);
            Log.i("gruppo", groupIDFromLink);
            Intent toGroupIntent = new Intent(getApplicationContext(), GroupActivity.class);
            toGroupIntent.putExtra("groupID", groupIDFromLink);
            toGroupIntent.putExtra("activity", DebtActivity.class.toString());
            uri = null;
            startActivity(toGroupIntent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            finish();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        final DatabaseReference dbMembers = databaseReference.child(DBNodes.MEMBERS);
        // Check if is a member of the group
        final ValueEventListener checkMembers = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(currentUser.getUid())){
                    Toast.makeText(getApplicationContext(), R.string.unauthorized, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();
                }
                dbMembers.removeEventListener(this);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        dbMembers.addValueEventListener(checkMembers);

        balances = new ArrayList<>();
        balancesPN = new ArrayList<>();
        creditors = new ArrayList<>();
        debtors = new ArrayList<>();
        members = new ArrayList<>();
        memberKey = new ArrayList<>();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final Group thisGroup = dataSnapshot.getValue(Group.class);
                getSupportActionBar().setTitle(thisGroup.getGroupName());
                final String currencyCode = thisGroup.getCurrencyCode();
                final DatabaseReference dbMembers = databaseReference.child(DBNodes.MEMBERS);
                dbMembers.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        memberKey = new ArrayList<>();
                        for (final DataSnapshot memberSnap : dataSnapshot.getChildren()) {
                            final String memberID = memberSnap.getKey();
                            memberKey.add(memberID);

                            // Check members
                            final DatabaseReference dbRefUsers = firebaseDatabase.getInstance().getReference(DBNodes.USERS).child(memberID);
                            dbRefUsers.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
                                        final String memberUsername = dataSnapshot.getValue(String.class);
                                        members.add(new User (memberID, memberUsername));
                                        final Query qExpense = databaseReference.child(DBNodes.EXPENSES).orderByChild(DBNodes.PAYED_BY).equalTo(memberID);
                                        qExpense.addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                float paid = memberSnap.child(DBNodes.PAID).getValue(Float.class);
                                                float old = paid, totalAmount = 0;
                                                float forEach=0;
                                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                    Expense tempExpense = snapshot.getValue(Expense.class);
                                                    old = old + tempExpense.getAmount();
                                                }

                                                balances.add(new Balance(memberUsername, old));
                                                balancesPN.add(new Balance(memberUsername, old));
                                                Log.i(LOG_TAG, "new balance added");

                                                for (int i = 0; i < balances.size(); i++) {
                                                    totalAmount = totalAmount + balances.get(i).getBalance();
                                                }
                                                forEach = totalAmount / balances.size();
                                                // Calculate real balance with already paid debts
                                                for (int i = 0; i<balances.size(); i++){
                                                    balancesPN.get(i).setBalance(balances.get(i).getBalance() - forEach);
                                                    if (balancesPN.get(i).getUsername().equals(currentUser.getDisplayName())){
                                                        totalDebt.setText(String.format("%.2f", Math.abs(balancesPN.get(i).getBalance())) + " " + currencyCode);
                                                        if (balancesPN.get(i).getBalance() >= 0) {
                                                            giveorReceiveLabel.setText(R.string.receiveLabel);
                                                        } else {
                                                            giveorReceiveLabel.setText(R.string.giveLabel);
                                                        }

                                                    }
                                                }
                                                balancesAdapter = new BalancesAdapter(getApplicationContext(), R.layout.item_balances, balancesPN, currencyCode);
                                                balances_list.setAdapter(balancesAdapter);
                                                Log.i(LOG_TAG, "Spesa totale: " + totalAmount + " a testa: " + forEach);

                                                // Split balances into positive and negative ones
                                                creditors = new ArrayList<>();
                                                debtors = new ArrayList<>();
                                                for (int i=0; i<balancesPN.size(); i++){
                                                    if (balancesPN.get(i).getBalance() < 0) {
                                                        debtors.add(new Balance(balancesPN.get(i).getUsername(), balancesPN.get(i).getBalance()));
                                                    }
                                                    if (balancesPN.get(i).getBalance() >= 0) {
                                                        creditors.add(new Balance(balancesPN.get(i).getUsername(), balancesPN.get(i).getBalance()));
                                                    }
                                                    Log.i("valori", balancesPN.get(i).getUsername());
                                                }

                                                Log.i(LOG_TAG, "Debtors: " + debtors.size());
                                                Log.i(LOG_TAG, "Creditors: " + creditors.size());

                                                // Debts algorithm calculation
                                                int i = 0, j = 0;
                                                boolean flag = false;
                                                debts = new ArrayList<>();
                                                while (j < debtors.size()){
                                                    if (creditors.get(i).getBalance() == 0){
                                                        ++i;
                                                    }
                                                    if (debtors.get(j).getBalance() == 0) {
                                                        ++j;
                                                    }

                                                    if (creditors.get(i).getBalance() < Math.abs(debtors.get(j).getBalance())){
                                                        Log.i("debito", debtors.get(j).getUsername() + " deve " + Math.abs(creditors.get(i).getBalance()) + " a " +
                                                                creditors.get(i).getUsername());
                                                        debts.add(new Debt(creditors.get(i).getUsername(), debtors.get(j).getUsername(), Math.abs(creditors.get(i).getBalance())));
                                                        debtors.get(j).setBalance(creditors.get(i).getBalance() + debtors.get(j).getBalance());
                                                        creditors.get(i).setBalance(0);

                                                        ++i;
                                                    }


                                                    if (flag == false){
                                                        if (creditors.get(i).getBalance() > Math.abs(debtors.get(j).getBalance())){
                                                            Log.i("debito", debtors.get(j).getUsername() + " deve " + Math.abs(debtors.get(j).getBalance()) + " a " +
                                                                    creditors.get(i).getUsername());
                                                            debts.add(new Debt(creditors.get(i).getUsername(), debtors.get(j).getUsername(), Math.abs(debtors.get(j).getBalance())));
                                                            creditors.get(i).setBalance(creditors.get(i).getBalance() + debtors.get(j).getBalance());
                                                            debtors.get(j).setBalance(0);

                                                            flag = true;
                                                            ++j;
                                                        }

                                                    }

                                                    if (flag == false) {

                                                        if (creditors.get(i).getBalance() == Math.abs(debtors.get(j).getBalance())){
                                                            Log.i("debito", debtors.get(j).getUsername() + " deve " + Math.abs(debtors.get(j).getBalance()) + " a " +
                                                                    creditors.get(i).getUsername());
                                                            debts.add(new Debt(creditors.get(i).getUsername(), debtors.get(j).getUsername(), Math.abs(debtors.get(j).getBalance())));
                                                            creditors.get(i).setBalance(0);
                                                            debtors.get(j).setBalance(0);

                                                            ++i;
                                                            ++j;
                                                        }

                                                    }

                                                    flag = false;

                                                }
                                                debtsAdapter = new DebtListAdapter(getApplicationContext(), R.layout.item_debt, debts, currencyCode);
                                                debts_list.setAdapter(debtsAdapter);
                                                qExpense.removeEventListener(this);
                                            }


                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });

                                    }
                                    dbRefUsers.removeEventListener(this);
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });



                        }
                        dbMembers.removeEventListener(this);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                databaseReference.removeEventListener(this);
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void startNotification(String topic, String title, String message) {
        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();
        try {
            notifcationBody.put("title", title);
            notifcationBody.put("message", message);
            notifcationBody.put("group", topic);
            notification.put("to", "/topics/" + topic);
            notification.put("data", notifcationBody);
        } catch (JSONException e) {
            Log.e("notification", e.getMessage());
        }
        sendNotification(notification);

    }

    private void sendNotification(JSONObject notification) {
        final String FCM_API = "https://fcm.googleapis.com/fcm/send";
        final String serverKey = "key=" + "AAAAzshXOZs:APA91bE0xhqeTzDxEJr_cGu7tYRB8QU1jFl4ChFaZWfUWi5jYo4MFz2yvmw0pGF8jsYHZoee2HSx2Q20Pb4rnABc94ieGvVNRWlNVHX-m7-hZIJUng48kpxU4i8HchALEJ6z8tITV2fK";
        final String contentType = "application/json";
        final String TAG = "NOTIFICATION TAG";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(FCM_API, notification,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "onResponse: " + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(TAG, "onErrorResponse: Didn't work");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", serverKey);
                params.put("Content-Type", contentType);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

}
