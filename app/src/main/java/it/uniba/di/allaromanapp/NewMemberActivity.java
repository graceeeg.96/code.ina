package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class NewMemberActivity extends AppCompatActivity {

    Toolbar toolbar;
    String groupID;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    Button shareLinkButton,startBroadcastingButton;
    Group thisGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_member);

        Intent intent = getIntent();
        groupID = intent.getStringExtra("groupID");
        databaseReference=firebaseDatabase.getInstance().getReference(DBNodes.GROUPS).child(groupID);

        toolbar = findViewById(R.id.toolbar9);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.new_member_label_fab);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        shareLinkButton = findViewById(R.id.btn_share_group);
        startBroadcastingButton=findViewById(R.id.btn_start_broadcasting);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                thisGroup = dataSnapshot.getValue(Group.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        shareLinkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.joinGroup));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.shareLinkMember, groupID, thisGroup.getGroupName(), groupID));
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_app_via)));
            }
        });


        startBroadcastingButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(NewMemberActivity.this, BluetoothSharingGroupActivity.class);
                intent.putExtra("groupID", groupID);
                startActivity(intent);
            }
        });


    }
}
