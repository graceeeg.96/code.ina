package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;


public class ForgotPasswordActivity extends AppCompatActivity {

    Button btn_recovery_password;
    EditText mailAddress;
    Toolbar toolbar;
    private FirebaseAuth mAuth;
    private final int MAIL_MAX_LENGHT = 40;
    private final int MAIL_MIN_LENGHT = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar =  findViewById(R.id.toolbar_recovery_password);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.recovery_your_password);

        mAuth = FirebaseAuth.getInstance();
        btn_recovery_password= findViewById(R.id.btn_recovery_password);
        mailAddress =findViewById(R.id.mailAddress);

        //recovery password
        btn_recovery_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userEmail = mailAddress.getText().toString().trim();
                final int mailLength = mailAddress.getText().length();
                boolean checkOk = true;
                if (mailLength < MAIL_MIN_LENGHT || TextUtils.isEmpty(userEmail)) {
                    checkOk = false;
                    mailAddress.setError(getText(R.string.mailTooSmall));
                    mailAddress.requestFocus();
                }
                if (mailLength > MAIL_MAX_LENGHT) {
                    checkOk = false;
                    mailAddress.setError(getText(R.string.mailTooLong));
                    mailAddress.requestFocus();
                } if(checkOk){
                    mAuth.sendPasswordResetEmail(userEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(ForgotPasswordActivity.this, R.string.reset_password, Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ForgotPasswordActivity.this, MainActivity.class));
                            }else{
                                String message= task.getException().getMessage();
                                Toast.makeText(ForgotPasswordActivity.this, R.string.reset_password_failed+ message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(getApplicationContext(), R.string.reset_password_email_failed, Toast.LENGTH_SHORT).show();
                }
            }
        });




        }

//back to MainActivity
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }



}
