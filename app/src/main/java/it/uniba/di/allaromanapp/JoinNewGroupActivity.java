package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JoinNewGroupActivity extends AppCompatActivity {

    EditText inputGroupID;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    Button joinGroupCodeButton;
    boolean isPrivateAccess;
    String codeAccess, groupID;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_new_group);

        new AdaptiveDarkMode(JoinNewGroupActivity.this);

        if (currentUser==null){
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }


        Toolbar toolbar = findViewById(R.id.toolbar4);
        Button btn_start_broadcasting = findViewById(R.id.btn_start_broadcasting);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        inputGroupID = findViewById(R.id.inputGroupID);
        joinGroupCodeButton = findViewById(R.id.joinGroupCodeButton);

        // groupID taken from bluetooth
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("groupID")) {
                groupID = intent.getStringExtra("groupID");
                inputGroupID.setText(groupID);
            }

        }

        joinGroupCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String groupID = inputGroupID.getText().toString();
                if (!groupID.isEmpty()){
                    joinGroup(inputGroupID.getText().toString());
                } else {
                    Toast.makeText(getApplicationContext(), R.string.groupNameTooSmall, Toast.LENGTH_SHORT).show();
                }

            }
        });


        btn_start_broadcasting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   Intent intent=new Intent(getApplicationContext(), BluetoothJoinGroupActivity.class);
                   startActivity(intent);
            }
        });


    }

    @Override
    protected void onStart(){
        super.onStart();
        uri = getIntent().getData();
        // groupID taken by dynamic link
        if (uri != null){
            List<String> params = uri.getPathSegments();
            String groupIDFromLink = params.get(params.size()-1);
            Log.i("gruppo", groupIDFromLink);
            inputGroupID.setText(groupIDFromLink);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (uri != null){
            Intent toGroupIntent = new Intent(getApplicationContext(), HomeActivity.class);
            uri = null;
            startActivity(toGroupIntent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void joinGroup(final String groupID){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference searchGroup = database.getReference(DBNodes.GROUPS).child(groupID);

        searchGroup.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    final Intent toGroupIntent = new Intent(getApplicationContext(), GroupActivity.class);
                    final Group thisGroup = dataSnapshot.getValue(Group.class);
                    toGroupIntent.putExtra("groupID", groupID);
                    isPrivateAccess = thisGroup.isPrivateAccess();
                    codeAccess = thisGroup.getcodeAccess();
                    final DatabaseReference groupMember = searchGroup.child(DBNodes.MEMBERS);
                    groupMember.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            // If already member
                            if (!dataSnapshot.hasChild(currentUser.getUid())){
                                if(isPrivateAccess) {
                                    AlertDialog.Builder builder=new AlertDialog.Builder(JoinNewGroupActivity.this);
                                    builder.setMessage(R.string.join_group_password);
                                    final EditText input = new EditText(JoinNewGroupActivity.this);
                                    LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    input.setLayoutParams(lp);
                                    // Prepare dialog message to insert the access code
                                    builder.setView(input);
                                    builder.setPositiveButton(R.string.ok_join_group, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            String password=input.getText().toString();
                                            if(password.equals(codeAccess)){
                                                Toast.makeText(getApplicationContext(), getString(R.string.groupJoinSuccess, thisGroup.getGroupName()), Toast.LENGTH_SHORT).show();
                                                FirebaseMessaging.getInstance().subscribeToTopic(groupID);
                                                startNotification(groupID, getString(R.string.newMemberTitle), getString(R.string.newMemberMsg, currentUser.getDisplayName(), thisGroup.getGroupName()));
                                                groupMember.child(currentUser.getUid()).child(DBNodes.ADMIN).setValue(false);
                                                groupMember.child(currentUser.getUid()).child(DBNodes.PAID).setValue(0);
                                                searchGroup.child(DBNodes.LOGS).push().setValue(new GroupLog(getString(R.string.join, currentUser.getDisplayName())));
                                                startActivity(toGroupIntent);
                                                finish();
                                            }else{
                                                Toast.makeText(getApplicationContext(), R.string.groupJoinFailed, Toast.LENGTH_SHORT).show();
                                            }


                                        }
                                    })
                                            .show();
                                }else{
                                    Toast.makeText(getApplicationContext(), getString(R.string.groupJoinSuccess, thisGroup.getGroupName()), Toast.LENGTH_SHORT).show();
                                    startNotification(groupID, getString(R.string.newMemberTitle), getString(R.string.newMemberMsg, currentUser.getDisplayName(), thisGroup.getGroupName()));
                                    groupMember.child(currentUser.getUid()).child(DBNodes.ADMIN).setValue(false);
                                    groupMember.child(currentUser.getUid()).child(DBNodes.PAID).setValue(0);
                                    searchGroup.child(DBNodes.LOGS).push().setValue(new GroupLog(getString(R.string.join, currentUser.getDisplayName())));
                                    startActivity(toGroupIntent);
                                    finish();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.alreadyInGroup, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                } else {
                    Toast.makeText(getApplicationContext(), R.string.groupJoinFailed, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void startNotification(String topic, String title, String message){
        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();
        try {
            notifcationBody.put("title", title);
            notifcationBody.put("message", message);
            notifcationBody.put("group", topic);
            notification.put("to", "/topics/" + topic);
            notification.put("data", notifcationBody);
        } catch (JSONException e) {
            Log.e("notification", e.getMessage() );
        }
        sendNotification(notification);

    }

    private void sendNotification(JSONObject notification) {
        final String FCM_API = "https://fcm.googleapis.com/fcm/send";
        final String serverKey = "key=" + "AAAAzshXOZs:APA91bE0xhqeTzDxEJr_cGu7tYRB8QU1jFl4ChFaZWfUWi5jYo4MFz2yvmw0pGF8jsYHZoee2HSx2Q20Pb4rnABc94ieGvVNRWlNVHX-m7-hZIJUng48kpxU4i8HchALEJ6z8tITV2fK";
        final String contentType = "application/json";
        final String TAG = "NOTIFICATION TAG";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(FCM_API, notification,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "onResponse: " + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(TAG, "onErrorResponse: Didn't work");
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", serverKey);
                params.put("Content-Type", contentType);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


}
