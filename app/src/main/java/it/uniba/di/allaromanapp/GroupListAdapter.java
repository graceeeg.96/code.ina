package it.uniba.di.allaromanapp;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;


public class GroupListAdapter extends BaseAdapter {
    private ArrayList<Group> groups; //data source of the list adapter
    private ArrayList<Long> membersNum;

    private Context mContext;

    //public constructor
    public GroupListAdapter(Context context, ArrayList<Group> groups, ArrayList<Long> membersNum) {
        this.mContext = context;
        this.groups = groups;
        this.membersNum = membersNum;
    }


    @Override
    public int getCount() {
        return groups.size();
    }

    @Override
    public Group getItem(int position) {
        return groups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Long getMembersNum(int position) {
        return membersNum.get(position);
    }

    @NonNull
    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {
        Group currentGroup =  getItem(position);
        Long currentMembersNum = getMembersNum(position);
        // Letter text group logo
        ColorGenerator generator = ColorGenerator.MATERIAL;
        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(currentGroup.getGroupName().charAt(0)).toUpperCase(), generator.getColor(position));

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(R.layout.item_group, parent, false);

        ImageView groupIcon = convertView.findViewById(R.id.group_icon);
        TextView groupNameLabel = convertView.findViewById(R.id.group_name);
        TextView number=convertView.findViewById(R.id.number_of_participants);
        number.setText(mContext.getResources().getQuantityString(R.plurals.numberOfParticipants, currentMembersNum.intValue(), currentMembersNum.intValue()));
        groupNameLabel.setText(currentGroup.getGroupName());
        groupIcon.setImageDrawable(drawable);
        return convertView;
    }
}
