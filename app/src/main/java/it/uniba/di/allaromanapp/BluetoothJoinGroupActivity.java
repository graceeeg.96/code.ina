package it.uniba.di.allaromanapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.UUID;


public class BluetoothJoinGroupActivity extends AppCompatActivity {


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    String groupID, BT_FLOW = "BT_FLOW_Client";
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    BluetoothAdapter adapter;
    Intent btEnablingIntent;
    int requestcodeForEnable;

    private boolean CONTINUE_READ_WRITE = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_join_group);
        Toolbar toolbar = findViewById(R.id.toolbar6);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.join_new_group);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btEnablingIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        requestcodeForEnable = 1;
        adapter = BluetoothAdapter.getDefaultAdapter();
        // Check for BT
        if(adapter!=null && !adapter.isEnabled()){
          startActivityForResult(btEnablingIntent,requestcodeForEnable);
        }

        registerReceiver(discoveryResult, new IntentFilter(BluetoothDevice.ACTION_FOUND));


        if(adapter != null && adapter.isDiscovering()){
            adapter.cancelDiscovery();
        }
        adapter.startDiscovery();
        ImageView bluetoothImage = findViewById(R.id.image_bluetooth);
       //add pulse animation for bluetoothImage
        ObjectAnimator pulse;
        pulse = ObjectAnimator.ofPropertyValuesHolder(bluetoothImage,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f));
        pulse.setDuration(300);
        pulse.setRepeatCount(ObjectAnimator.INFINITE);
        pulse.setRepeatMode(ObjectAnimator.REVERSE);
        pulse.start();

        Button refreshButton = findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{unregisterReceiver(discoveryResult);}catch(Exception e){e.printStackTrace();}
                if(socket != null){
                    try{
                        is.close();
                        os.close();
                        socket.close();
                        CONTINUE_READ_WRITE = false;
                    }catch(Exception e){}
                }
                registerReceiver(discoveryResult, new IntentFilter(BluetoothDevice.ACTION_FOUND));
                BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                if(adapter != null && adapter.isDiscovering()){
                    adapter.cancelDiscovery();
                }
                adapter.startDiscovery();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==requestcodeForEnable){
            if(requestCode==RESULT_OK){
                Log.i(BT_FLOW, "Bluetooth is Enable");
            }else if(requestCode==RESULT_CANCELED){
                Log.i(BT_FLOW, "Bluetooth is Canceled");
            }
        }
    }

    private void disableBT(){
        adapter = BluetoothAdapter.getDefaultAdapter();
        if(adapter.isEnabled()){
            adapter.disable();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        disableBT();
        adapter.cancelDiscovery();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                disableBT();
                adapter.cancelDiscovery();

            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            unregisterReceiver(discoveryResult);
        }catch(Exception e){
            e.printStackTrace();
        }
        if(socket != null){
            try{
                is.close();
                os.close();
                socket.close();
                CONTINUE_READ_WRITE = false;
            }catch(Exception e){}
        }
    }

    private BluetoothSocket socket;
    private OutputStreamWriter os;
    private InputStream is;
    private BluetoothDevice remoteDevice;
    private BroadcastReceiver discoveryResult = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            unregisterReceiver(this);
            remoteDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            new Thread(reader).start();
        }
    };

    private Runnable reader = new Runnable() {

        @Override
        public void run() {
            try {
                Log.i(BT_FLOW, "Found: " + remoteDevice.getName());
                UUID uuid = UUID.fromString("4e5d48e0-75df-11e3-981f-0800200c9a66");
                socket = remoteDevice.createRfcommSocketToServiceRecord(uuid);
                socket.connect();
                Log.i(BT_FLOW, "Connected...");
                os = new OutputStreamWriter(socket.getOutputStream());
                is = socket.getInputStream();
                Log.i(BT_FLOW, String.valueOf(CONTINUE_READ_WRITE));
                int bufferSize = 1024;
                int bytesRead = -1;
                byte[] buffer = new byte[bufferSize];
                //Keep reading the messages while connection is open
                while(CONTINUE_READ_WRITE){
                    final StringBuilder sb = new StringBuilder();
                    bytesRead = is.read(buffer);
                    if (bytesRead != -1) {
                        String result = "";
                        while ((bytesRead == bufferSize) && (buffer[bufferSize-1] != 0)){
                            result = result + new String(buffer, 0, bytesRead - 1);
                            bytesRead = is.read(buffer);
                        }
                        result = result + new String(buffer, 0, bytesRead - 1);
                        sb.append(result);
                    }

                    Log.i(BT_FLOW, "Read: " + sb.toString());

                    //Show message on UIThread
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new Thread(writter).start();
                            Intent joinGroupIntent = new Intent(getApplicationContext(), JoinNewGroupActivity.class);
                            joinGroupIntent.putExtra("groupID", sb.toString());
                            startActivity(joinGroupIntent);
                            finish();
                        }
                    });
                }
            } catch (IOException e) {e.printStackTrace();}
        }
    };

    // Write data to server
    private Runnable writter = new Runnable() {

        @Override
        public void run() {
            while (CONTINUE_READ_WRITE) {
                try {
                    os.write(getString(R.string.newMemberJoining, currentUser.getDisplayName()) + "\n");
                    os.flush();
                    Thread.sleep(2000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };
}

