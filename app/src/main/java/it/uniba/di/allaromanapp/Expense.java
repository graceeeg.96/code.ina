package it.uniba.di.allaromanapp;

public class Expense {
    String title, payedBy, date, time;
    float amount;

    public Expense(String title, float amount, String payedBy, String date, String time){
        this.title = title;
        this.amount = amount;
        this.payedBy = payedBy;
        this.date=date;
        this.time=time;
    }

    public Expense(){

    }




    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPayedBy() {
        return payedBy;
    }

    public void setPayedBy(String payedBy) {
        this.payedBy = payedBy;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
