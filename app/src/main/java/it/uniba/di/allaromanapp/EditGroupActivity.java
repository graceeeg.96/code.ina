package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EditGroupActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener,CompoundButton.OnCheckedChangeListener {

    Toolbar toolbar;
    private Switch privateEditGroupSwitch;
    private EditText titleGroupName, newPassword;
    private TextView defaultCurrency, defaultCategory;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private final int TITLE_MAX_LENGHT = 20;
    private final int TITLE_MIN_LENGHT = 3;
    private final int ACCESSCODE_MAX_LENGHT = 20;
    private final int ACCESSCODE_MIN_LENGHT = 4;
    private ImageView currencyEditMenuArrow, categoryEditMenuArrow;
    String codeAccess, title, groupID;
    boolean isPrivateAccess;
    Button btn_edit_group;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);

        Toolbar toolbar = findViewById(R.id.toolbar12);
        setSupportActionBar(toolbar);
        EditGroupActivity.this.setTitle(R.string.editGroupLabel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        new AdaptiveDarkMode(EditGroupActivity.this);

        privateEditGroupSwitch = findViewById(R.id.privateEditGroupSwitch);
        titleGroupName = findViewById(R.id.titleGroupName);
        newPassword =findViewById(R.id.inputEditNewPasswordGroup);
        defaultCategory = findViewById(R.id.defaultEditCategory);
        defaultCurrency = findViewById(R.id.defaultEditCurrency);
        currencyEditMenuArrow=findViewById(R.id.currencyEditMenuArrow);
        categoryEditMenuArrow=findViewById(R.id.categoryEditMenuArrow);
        btn_edit_group = findViewById(R.id.btn_edit_group);
        final ProgressBar updating = findViewById(R.id.updating);

        Intent intent = getIntent();
        groupID = intent.getStringExtra("groupID");
        databaseReference = firebaseDatabase.getInstance().getReference(DBNodes.GROUPS).child(groupID);
        if (privateEditGroupSwitch != null) {
            privateEditGroupSwitch.setOnCheckedChangeListener(this);
        }


        currencyEditMenuArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCurrenciesPopupMenu(view);
            }
        });

        categoryEditMenuArrow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                showCategoriesPopupMenu(view);
            }
        });


        btn_edit_group.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                btn_edit_group.setVisibility(View.GONE);
                updating.setVisibility(View.VISIBLE);
                final String password = newPassword.getText().toString().trim();
                title = titleGroupName.getText().toString().trim();
                String category = defaultCategory.getText().toString();
                String currency = defaultCurrency.getText().toString();
                boolean checkTitle = true;

                if (!TextUtils.isEmpty((title))) {
                    if (title.length() < TITLE_MIN_LENGHT) {
                        checkTitle = false;
                        titleGroupName.setError(getText(R.string.groupNameTooSmall));
                        titleGroupName.requestFocus();
                    }

                    if (title.length() > TITLE_MAX_LENGHT) {
                        checkTitle = false;
                        titleGroupName.setError(getText(R.string.groupNameTooLong));
                        titleGroupName.requestFocus();
                    }
                } else {
                    checkTitle = false;
                }

                // Group title update
                if (checkTitle) {
                    closeKeyboard();
                    DatabaseReference db= firebaseDatabase.getInstance().getReference(DBNodes.GROUPS).child(groupID).child(DBNodes.GROUP_NAME);
                    db.setValue(title).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            titleGroupName.getText().clear();
                            titleGroupName.setHint(title);

                        }
                    });

                }

                // Password (if private group) update
                if(privateEditGroupSwitch.isChecked()){
                    boolean checkAccessCode = true;
                    if (!TextUtils.isEmpty((password))) {
                        if (password.length() < ACCESSCODE_MIN_LENGHT) {
                            checkAccessCode = false;
                            newPassword.setError(getText(R.string.passwordTooSmall));
                            newPassword.requestFocus();
                        }

                        if (password.length() > ACCESSCODE_MAX_LENGHT) {
                            checkAccessCode = false;
                            newPassword.setError(getText(R.string.passwordTooLong));
                            newPassword.requestFocus();
                        }
                    } else {
                        checkAccessCode = false;
                    }

                    if (checkAccessCode){
                        closeKeyboard();
                        DatabaseReference codeAccessRef = databaseReference.child(DBNodes.CODE_ACCESS);
                        codeAccessRef.setValue(password).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                DatabaseReference dbPrivateAccess = databaseReference.child(DBNodes.PRIVATE_ACCESS);
                                dbPrivateAccess.setValue(true);
                                newPassword.getText().clear();
                                newPassword.setHint(password);
                            }
                        });
                    }
                } else {
                    DatabaseReference dbPrivateAccess = databaseReference.child(DBNodes.PRIVATE_ACCESS);
                    dbPrivateAccess.setValue(false);
                }

                // Category update
                DatabaseReference categoryRef= databaseReference.child(DBNodes.CATEGORY);
                categoryRef.setValue(category);

                // Currency code update
                DatabaseReference currencyRef= databaseReference.child(DBNodes.CURRENCY_CODE);
                currencyRef.setValue(currency);
                updating.setVisibility(View.GONE);
                btn_edit_group.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(),R.string.editCompleted, Toast.LENGTH_SHORT).show();
            }
        });
    }





    @Override
    protected void onStart() {
        super.onStart();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final Group thisGroup = dataSnapshot.getValue(Group.class);
                titleGroupName.setHint(thisGroup.getGroupName());
                defaultCategory.setText(thisGroup.getCategory());
                defaultCurrency.setText(thisGroup.getCurrencyCode());
                codeAccess = thisGroup.getcodeAccess();
                isPrivateAccess=thisGroup.isPrivateAccess();
                if(isPrivateAccess){
                    privateEditGroupSwitch.setChecked(true);
                    }
                }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
           if (isChecked ) {
               newPassword.setVisibility(View.VISIBLE);
               newPassword.setVisibility(View.VISIBLE);
               newPassword.setHint(codeAccess);
        } else {
               newPassword.setVisibility(View.INVISIBLE);
               newPassword.setVisibility(View.INVISIBLE);
        }
    }


    public void showCategoriesPopupMenu(View v) {
        PopupMenu popup1 = new PopupMenu(this, v);
        popup1.setOnMenuItemClickListener(this);
        popup1.inflate(R.menu.popup_categories_menu);
        popup1.show();
    }

    public void showCurrenciesPopupMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.popup_currencies_menu);
        popup.show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        // Currencies menu
        switch(menuItem.getItemId()){
            case R.id.EUR:
                defaultCurrency.setText("EUR");
                return true;
            case R.id.USD:
                defaultCurrency.setText("USD");
                return true;
            case R.id.JPY:
                defaultCurrency.setText("JPY");
                return true;
            case R.id.GBP:
                defaultCurrency.setText("GBP");
                return true;
            case R.id.CHF:
                defaultCurrency.setText("CHF");
                return true;
            case R.id.AUD:
                defaultCurrency.setText("AUD");
                return true;
            case R.id.other:
                startActivityForResult(new Intent(this, CurrenciesActivity.class), 2);
                return true;
            // Categories menu
            case R.id.travelLabel:
                defaultCategory.setText(R.string.DefaultCategoryLabel);
                return true;
            case R.id.coupleLabel:
                defaultCategory.setText(R.string.CoupleLabel);
                return true;
            case R.id.sharedHouseLabel:
                defaultCategory.setText(R.string.SharedHouseLabel);
                return true;
            case R.id.EventLabel:
                defaultCategory.setText(R.string.EventLabel);
                return true;
            case R.id.ProjectLabel:
                defaultCategory.setText(R.string.ProjectLabel);
                return true;
            case R.id.otherLabel1:
                defaultCategory.setText(R.string.otherLabel);
                return true;
            default:
                return false;

        }
    }

    private void closeKeyboard() {
        View view = EditGroupActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) EditGroupActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }




}
