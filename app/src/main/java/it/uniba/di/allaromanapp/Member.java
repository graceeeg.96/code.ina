package it.uniba.di.allaromanapp;

public class Member {
    private String username;
    private boolean admin;

    public Member(String username, boolean admin){
        this.username = username;
        this.admin = admin;
    }

    public Member(){

    }


    public boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
