package it.uniba.di.allaromanapp;

public final class DBNodes {

    public static final String GROUPS = "groups";
    public static final String MEMBERS = "members";
    public static final String USERS = "users";
    public static final String LOGS = "logs";
    public static final String EXPENSES = "expenses";
    public static final String ADMIN = "admin";
    public static final String PAID = "paid";
    public static final String PAYED_BY = "payedBy";
    public static final String GROUP_NAME = "groupName";
    public static final String CATEGORY = "category";
    public static final String CURRENCY_CODE = "currencyCode";
    public static final String CODE_ACCESS = "codeAccess";
    public static final String PRIVATE_ACCESS = "privateAccess";

    private DBNodes() { }
}