package it.uniba.di.allaromanapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.media.Image;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WalkthroughActivity extends AppCompatActivity {

    private static final int MAX_STEP=3;
    private int current_step=1;
    private TextView presentationLabel, backLabel, nextLabel;
    ImageView imageArrowLeft, imageView;
    String SP = "walkthroughDone";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        boolean walkthroughDone = sp.getBoolean(SP, false); // Second parameter is the default value.
        // If walkthrough already done, start login activity
        if (walkthroughDone){
            Intent intent_login = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent_login);
        }
        presentationLabel=findViewById(R.id.presentation_label);
        backLabel=findViewById(R.id.back_label);
        imageArrowLeft=findViewById(R.id.image_arrow_left);
        imageView=findViewById(R.id.image_View_walkthrough);
        nextLabel=findViewById(R.id.next_label);
        backLabel.setVisibility(View.INVISIBLE);
        imageArrowLeft.setVisibility(View.INVISIBLE);


        (findViewById(R.id.layout_back)).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                backStep(current_step);
                buttonProgressDots(current_step);
            }
        });

        (findViewById(R.id.layout_next)).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                nextStep(current_step);
                if (current_step<=MAX_STEP){
                    buttonProgressDots(current_step);
                }



            }
        });

        buttonProgressDots(current_step);

        }

    private void backStep(int progress) {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(1000);
        if (progress > 1) {
            progress--;
            current_step = progress;

            if(current_step==1){
                String str= getString(R.string.presentation_label);
                presentationLabel.setText(str);
                imageView.setAnimation(fadeIn);
                imageView.setImageResource(R.drawable.ic_walkthrough_1);
                backLabel.setVisibility(View.INVISIBLE);
                imageArrowLeft.setVisibility(View.INVISIBLE);
                nextLabel.setText(R.string.next_steppers);
            }else if(current_step==2){
                String str_one= getString(R.string.presentation_label_one);
                presentationLabel.setText(str_one);
                imageView.setAnimation(fadeIn);
                imageView.setImageResource(R.drawable.ic_walkthrough_2);
                backLabel.setVisibility(View.VISIBLE);
                imageArrowLeft.setVisibility(View.VISIBLE);
                nextLabel.setText(R.string.next_steppers);
            }else if(current_step==3){
                String str_two=getString(R.string.presentation_label_two);
                presentationLabel.setText(str_two);
                imageView.setAnimation(fadeIn);
                imageView.setImageResource(R.drawable.ic_walkthrough_3);
                backLabel.setVisibility(View.VISIBLE);
                imageArrowLeft.setVisibility(View.VISIBLE);
                nextLabel.setText(R.string.start_steppers);
            }

        }
    }


        private void nextStep(int progress){
            Animation fadeIn = new AlphaAnimation(0, 1);
            fadeIn.setInterpolator(new DecelerateInterpolator());
            fadeIn.setDuration(1000);
            if (progress < MAX_STEP) {
                progress++;
                current_step = progress;
                if (current_step == 1) {
                    String str = getString(R.string.presentation_label);
                    presentationLabel.setText(str);
                    imageView.setAnimation(fadeIn);
                    imageView.setImageResource(R.drawable.ic_walkthrough_1);
                    backLabel.setVisibility(View.INVISIBLE);
                    imageArrowLeft.setVisibility(View.INVISIBLE);
                    nextLabel.setText(R.string.next_steppers);
                } else if (current_step == 2) {
                    String str_one = getString(R.string.presentation_label_one);
                    presentationLabel.setText(str_one);
                    backLabel.setVisibility(View.VISIBLE);
                    imageArrowLeft.setVisibility(View.VISIBLE);
                    imageView.setAnimation(fadeIn);
                    imageView.setImageResource(R.drawable.ic_walkthrough_2);
                    nextLabel.setText(R.string.next_steppers);
                } else if (current_step == 3) {
                    String str_two = getString(R.string.presentation_label_two);
                    presentationLabel.setText(str_two);
                    backLabel.setVisibility(View.VISIBLE);
                    imageView.setAnimation(fadeIn);
                    imageView.setImageResource(R.drawable.ic_walkthrough_3);
                    imageArrowLeft.setVisibility(View.VISIBLE);
                    nextLabel.setText(R.string.start_steppers);

                }

            } else {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sp.edit();
                editor.putBoolean(SP, true);
                editor.apply();
                Intent intent_login = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent_login);
                finish();
            }
        }



        private void buttonProgressDots(int current_index){
        current_index--;
        LinearLayout dotsLayout = findViewById(R.id.layout_dots);
        ImageView[] dots=new ImageView[MAX_STEP];
        dotsLayout.removeAllViews();
        for(int i=0; i<dots.length; i++){
            dots[i]=new ImageView(this);
            int width_height=15;
            LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(getResources().getColor(R.color.disable_forground_grey), PorterDuff.Mode.SRC_IN);
            dotsLayout.addView(dots[i]);
         }

            dots[current_index].setImageResource(R.drawable.shape_circle);
            dots[current_index].setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        }

    }

