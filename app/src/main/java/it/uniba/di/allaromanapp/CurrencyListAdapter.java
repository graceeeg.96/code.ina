package it.uniba.di.allaromanapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;


import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CurrencyListAdapter extends ArrayAdapter<Currency> {

    private Context mContext;
    int mResource;

    public CurrencyListAdapter(Context context, int resource, ArrayList<Currency> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {
        String code = getItem(position).getCode();
        String currency = getItem(position).getCurrency();

        LayoutInflater inflater=LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        TextView codeLabel = convertView.findViewById(R.id.Code);
        TextView currencyLabel = convertView.findViewById(R.id.Currency);

        codeLabel.setText(code);
        currencyLabel.setText(currency);

        return convertView;

    }
}
