package it.uniba.di.allaromanapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;


public class GroupLogsListAdapter extends ArrayAdapter<GroupLog> {

    private Context mContext;
    int mResource;

    public GroupLogsListAdapter(@NonNull Context context, int resource, ArrayList<GroupLog> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String logMsg=getItem(position).getLogMsg();
        String date=getItem(position).getDate();
        String time=getItem(position).getTime();
        ColorGenerator generator = ColorGenerator.MATERIAL;
        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(logMsg.charAt(0)).toUpperCase(), generator.getColor(position));
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        ImageView userIconLabel= convertView.findViewById(R.id.user_log_message);
        TextView logMsgLabel=convertView.findViewById(R.id.log_message);
        TextView dateTimeHistoricalLabel=convertView.findViewById(R.id.dateTime_log_label);
        logMsgLabel.setText(logMsg);
        userIconLabel.setImageDrawable(drawable);
        dateTimeHistoricalLabel.setText(date+ " "+ getContext().getResources().getString(R.string.at_time) + " "+ time);
        return convertView;
    }
}
