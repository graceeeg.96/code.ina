package it.uniba.di.allaromanapp;

public class Debt {
    String creditorUsername, debtorUsername;
    float debtAmount;

    public Debt(String creditorUsername, String debtorUsername, float debtAmount){

        this.creditorUsername= creditorUsername;
        this.debtorUsername = debtorUsername;
        this.debtAmount = debtAmount;
    }

    public String getCreditorUsername() {
        return creditorUsername;
    }

    public void setCreditorUsername(String creditorUsername) {
        this.creditorUsername = creditorUsername;
    }

    public String getDebtorUsername() {
        return debtorUsername;
    }

    public void setDebtorUsername(String debtorUsername) {
        this.debtorUsername = debtorUsername;
    }

    public float getDebtAmount() {
        return debtAmount;
    }

    public void setDebtAmount(float debtAmount) {
        this.debtAmount = debtAmount;
    }
}
