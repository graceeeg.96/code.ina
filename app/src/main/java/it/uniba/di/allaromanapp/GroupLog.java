package it.uniba.di.allaromanapp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GroupLog {

    private String logMsg;
    String date;
    String time;

    public GroupLog(String logMsg) {
        DateFormat dfTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()); // Format time
        DateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()); // Format date
        this.logMsg = logMsg;
        this.time = dfTime.format(Calendar.getInstance().getTime());
        this.date = dfDate.format(Calendar.getInstance().getTime());


    }

    public GroupLog(){

    }

    public String getLogMsg() {
        return logMsg;
    }

    public void setLogMsg(String logMsg) {
        this.logMsg = logMsg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
