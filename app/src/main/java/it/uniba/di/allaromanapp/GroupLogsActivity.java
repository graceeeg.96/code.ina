package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class GroupLogsActivity extends AppCompatActivity {
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    String groupID;
    ListView groupLogsList;
    ArrayList<GroupLog> logs = new ArrayList<>();
    GroupLogsListAdapter groupLogsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_logs);
        Toolbar toolbar = findViewById(R.id.toolbarGroupLogs);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.groupLogsActivityTitle);
        Intent intent = getIntent();
        groupID = intent.getStringExtra("groupID");
        databaseReference = firebaseDatabase.getInstance().getReference(DBNodes.GROUPS).child(groupID).child(DBNodes.LOGS);
        groupLogsList = findViewById(R.id.groupLogsList);
        new AdaptiveDarkMode(GroupLogsActivity.this);
    }


    // Retrieve log data and prepare adapter
    public void onStart(){
        super.onStart();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    logs = new ArrayList<>();
                    for (DataSnapshot logsSnap : dataSnapshot.getChildren()){
                        logs.add(logsSnap.getValue(GroupLog.class));
                    }
                    Collections.reverse(logs);
                    groupLogsListAdapter  = new GroupLogsListAdapter(getApplicationContext(), R.layout.item_log, logs);
                    groupLogsList.setAdapter(groupLogsListAdapter);
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
