package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;


public class MainActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    TextView forgotPassword;
    Button registrationButton, btn_login;
    CheckBox showHidePswCheckbox;
    ProgressBar progressBar2;
    EditText password, mail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();

        forgotPassword = findViewById(R.id.forgotPassword);
        registrationButton = findViewById(R.id.btn_registration);
        btn_login = findViewById(R.id.btnLogin);
        showHidePswCheckbox = findViewById(R.id.showHidePswCheckbox);
        progressBar2 = findViewById(R.id.progressBar2);
        password = findViewById(R.id.Password);
        mail = findViewById(R.id.mailAddress);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String mailText = mail.getText().toString().trim();
                final String passwordText = password.getText().toString().trim();
                // Field check
                if (TextUtils.isEmpty(passwordText) || TextUtils.isEmpty(mailText)) {
                    Toast.makeText(getApplicationContext(), R.string.LoginFailed, Toast.LENGTH_SHORT).show();
                } else {
                    progressBar2.setVisibility(View.VISIBLE);
                    // Try to sign in
                    mAuth.signInWithEmailAndPassword(mailText, passwordText).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), R.string.LoginSuccess, Toast.LENGTH_SHORT).show();
                                VerifyEmailAddress();
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.LoginFailed, Toast.LENGTH_SHORT).show();
                                progressBar2.setVisibility(View.GONE);
                            }
                        }
                    });

                }
            }
        });


        // Go to SignupActivity
        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registrationIntent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(registrationIntent);
            }

        });


        // Go to Forgot Password activity
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent forgotPasswordIntent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(forgotPasswordIntent);

            }
        });

        showHidePswCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if (value) {
                    //Show Password
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    //Hide Password
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());

                }
            }
        });
    }

    @Override
    public void onStart(){
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null && currentUser.isEmailVerified()){
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (!task.isSuccessful()) {
                        Log.w("token", "getInstanceId failed", task.getException());
                        return;
                    }
                }
            });
            // Topic used to send notification to the individual user
            FirebaseMessaging.getInstance().subscribeToTopic(currentUser.getUid());
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }
        // Check if reset password requested by user
        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            if (extras.getBoolean("Reset Password", false)){
                Toast.makeText(getApplicationContext(), getString(R.string.pswMailSent), Toast.LENGTH_LONG).show();
            }
        }


    }


    private void VerifyEmailAddress () {
        boolean emailAdressChecker;
        FirebaseUser user = mAuth.getCurrentUser();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        emailAdressChecker = user.isEmailVerified();
        if (emailAdressChecker) {
            DatabaseReference databaseReference = firebaseDatabase.getReference(DBNodes.USERS).child(user.getUid());
            databaseReference.setValue(user.getDisplayName());
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        } else {
            Toast.makeText(getApplicationContext(), R.string.verify_email_address, Toast.LENGTH_SHORT).show();
            progressBar2.setVisibility(View.GONE);
        }
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
