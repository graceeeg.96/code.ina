package it.uniba.di.allaromanapp;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new AdaptiveDarkMode(HomeActivity.this);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerStateChanged(int newState) {
                if (newState == DrawerLayout.STATE_SETTLING && !drawer.isDrawerOpen(GravityCompat.START)) {
                    updateNavHeader();
                    closeKeyboard();
                }
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft=fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container, new HomeFragment(),"home").commit();

    }

    public void onBackPressed() {
        Fragment home = getSupportFragmentManager().findFragmentByTag("home");
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (home == null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft=fragmentManager.beginTransaction();
                getSupportActionBar().setTitle(R.string.app_name);
                ft.replace(R.id.fragment_container, new HomeFragment(),"home").commit();
            } else {
                this.finishAffinity();
            }
        }


    }


    public boolean onNavigationItemSelected(MenuItem item){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft=fragmentManager.beginTransaction();
        int id = item.getItemId();
        switch (id){
            case R.id.nav_home:
                getSupportActionBar().setTitle(R.string.app_name);
                ft.replace(R.id.fragment_container, new HomeFragment(), "home").commit();
                break;
            case R.id.nav_logout:
                logout();
                break;
            case R.id.nav_email:
                getSupportActionBar().setTitle(R.string.contacts);
                ft.replace(R.id.fragment_container, new ContactFragment(), "contacts").commit();
                break;
            case R.id.nav_settings_app:
                getSupportActionBar().setTitle(R.string.settings_app);
                ft.replace(R.id.fragment_container, new SettingsAppFragment(), "settingsApp").commit();
                break;
            case R.id.nav_settings_profile:
                getSupportActionBar().setTitle(R.string.settings_profile);
                ft.replace(R.id.fragment_container, new SettingsAccountFragment(), "settingsAccount").commit();
                break;
            case R.id.nav_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_app_subject));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.share_app_message));
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_app_via)));
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return false;
    }


private void logout(){
    FirebaseAuth.getInstance().signOut();
    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
   }

   public void updateNavHeader(){
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = headerView.findViewById(R.id.name_navigation);
        TextView navMail = headerView.findViewById(R.id.mail_navigation);
        navMail.setText(currentUser.getEmail());
        navUsername.setText(currentUser.getDisplayName());

        // Letter text user logo
        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(currentUser.getDisplayName().charAt(0)).toUpperCase(), R.color.colorAccent);
        ImageView image = headerView.findViewById(R.id.image_view);
        image.setImageDrawable(drawable);
   }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}



