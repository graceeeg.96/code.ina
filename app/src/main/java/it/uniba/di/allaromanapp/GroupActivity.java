package it.uniba.di.allaromanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

public class GroupActivity extends AppCompatActivity {

    private ImageSwitcher sw;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    private TextView totalSpent, totalSpentLabel, noExpenseLabel;
    ImageView imageView, pigNoExpense;
    ListView expense_list;
    ArrayList<String> expensesKey;
    ArrayList<Expense> expenses;
    FloatingActionMenu floatingActionMenu;
    FloatingActionButton newMember, newExpense;
    ExpenseListAdapter expenseListAdapter;
    String groupID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        Intent intent = getIntent();
        groupID = intent.getStringExtra("groupID");
        databaseReference = firebaseDatabase.getInstance().getReference(DBNodes.GROUPS).child(groupID);
        FirebaseMessaging.getInstance().subscribeToTopic(groupID);

        Toolbar toolbar = findViewById(R.id.toolbar5);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        new AdaptiveDarkMode(GroupActivity.this);

        Button btn_debts = findViewById(R.id.btn_debts_to_be_paid);
        totalSpent = findViewById(R.id.numberTotalSpentLabel);
        totalSpentLabel = findViewById(R.id.totalSpentLabel);
        noExpenseLabel = findViewById(R.id.noExpenseLabel);
        expense_list =findViewById(R.id.expense_list);

        floatingActionMenu = findViewById(R.id.floatingActionMenu);
        newExpense =findViewById(R.id.fabNewExpense);
        newMember=findViewById(R.id.floatingActionNewMember);

        sw = findViewById(R.id.image_switcher_category);
        pigNoExpense = findViewById(R.id.pig_noExpense);

        // Group category image responsive
        sw.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                imageView = new ImageView(getApplicationContext());
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                return imageView;
            }
        });
        //discover debts with this button
        btn_debts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseReference dbExpenseRef = databaseReference.child(DBNodes.EXPENSES);
                dbExpenseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            Intent intentExpense=new Intent(getApplicationContext(),DebtActivity.class);
                            intentExpense.putExtra("groupID", groupID);
                            startActivity(intentExpense);
                        }else{
                            Toast.makeText(getApplicationContext(), R.string.not_have_expense, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });


        // FAB submenu
       newExpense.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               floatingActionMenu.close(true);
               Intent intent=new Intent(GroupActivity.this, NewExpenseActivity.class);
               intent.putExtra("groupID", groupID);
               startActivity(intent);
           }
       });
       newMember.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               floatingActionMenu.close(true);
               Intent intent=new Intent(GroupActivity.this, NewMemberActivity.class);
               intent.putExtra("groupID", groupID);
               startActivity(intent);
           }
       });

        //delete an expense
       expense_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
               final DatabaseReference dbExpenseRef = databaseReference.child(DBNodes.EXPENSES);
                     final int which_item = position;
                     new AlertDialog.Builder(GroupActivity.this)
                             .setIcon(android.R.drawable.ic_delete)
                             .setTitle(R.string.deleteExpenseTitle)
                             .setMessage(R.string.deleteExpenseMsg)
                             .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                 @Override
                                 public void onClick(DialogInterface dialogInterface, int i) {
                                     expenseListAdapter.notifyDataSetChanged();
                                     dbExpenseRef.child(expensesKey.get(which_item)).removeValue();
                                     Toast.makeText(getApplicationContext(), R.string.expenseRemoved, Toast.LENGTH_SHORT).show();
                                     expenses.remove(expenses.get(which_item));
                                     expensesKey.remove(expensesKey.get(which_item));
                                 }
                             })
                             .setNegativeButton(R.string.cancel, null)
                             .show();
           }
       });

    }


    //the options menu changes according to the type of user
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.options_menu_group, menu);
        databaseReference = firebaseDatabase.getInstance().getReference(DBNodes.GROUPS).child(groupID);
        DatabaseReference dbRef = databaseReference.child(DBNodes.MEMBERS + "/" + currentUser.getUid());
        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Add only admin functions to menu
                if (dataSnapshot.child(DBNodes.ADMIN).getValue(Boolean.class)){
                    menu.add(0,3,0,R.string.editGroupLabel);
                    menu.add(0,4,0,R.string.deleteGroup);
                } else {
                    // Add only member functions to menu
                    if (!dataSnapshot.child(DBNodes.ADMIN).getValue(Boolean.class)){
                        menu.add(0,5,0, R.string.leaveGroup);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return true;
    }

    @Override
    protected void onPause(){
        super.onPause();
        expensesKey = new ArrayList<>();
        expenses = new ArrayList<>();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent fromDebtUri = getIntent();
        if (fromDebtUri.getExtras().getString("activity", "").equals(DebtActivity.class.toString())){
            Intent toGroupIntent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(toGroupIntent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            finish();
        } else {
            super.onBackPressed();
        }


    }


    //view expense list
    @Override
    protected void onStart() {
        super.onStart();
        expensesKey = new ArrayList<>();
        expenses = new ArrayList<>();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    final Group thisGroup = dataSnapshot.getValue(Group.class);
                    getSupportActionBar().setTitle(thisGroup.getGroupName());
                    setImage(thisGroup.getCategory());
                    final DatabaseReference dbExpenseRef = databaseReference.child(DBNodes.EXPENSES);
                    dbExpenseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                pigNoExpense.setVisibility(View.GONE);
                                noExpenseLabel.setVisibility(View.GONE);
                                expense_list.setVisibility(View.VISIBLE);
                                totalSpent.setVisibility(View.VISIBLE);
                                totalSpentLabel.setVisibility(View.VISIBLE);
                                float totalSpentSum = 0;
                                expensesKey = new ArrayList<>();
                                expenses = new ArrayList<>();
                                for (final DataSnapshot expenseSnap : dataSnapshot.getChildren()) {
                                    final Expense tempExpense = expenseSnap.getValue(Expense.class);
                                    expensesKey.add(expenseSnap.getKey());
                                    final DatabaseReference dbRefUser = firebaseDatabase.getInstance().getReference(DBNodes.USERS).child(tempExpense.payedBy);
                                    dbRefUser.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()){
                                                Log.i("username", dataSnapshot.getValue(String.class));
                                                tempExpense.setPayedBy(dataSnapshot.getValue(String.class));
                                                expenses.add(tempExpense);
                                                expenseListAdapter = new ExpenseListAdapter(getBaseContext(), R.layout.item_expense, expenses, thisGroup.getCurrencyCode());
                                                expense_list.setAdapter(expenseListAdapter);
                                            }
                                            dbRefUser.removeEventListener(this);
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                    totalSpentSum += expenseSnap.getValue(Expense.class).getAmount();

                                }

                                totalSpent.setText(String.format("%.2f",totalSpentSum) + " " + thisGroup.getCurrencyCode());
                            } else {
                                pigNoExpense.setVisibility(View.VISIBLE);
                                noExpenseLabel.setVisibility(View.VISIBLE);
                                expense_list.setVisibility(View.GONE);
                                totalSpent.setVisibility(View.GONE);
                                totalSpentLabel.setVisibility(View.GONE);
                            }
                            dbExpenseRef.removeEventListener(this);


                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
                databaseReference.removeEventListener(this);
            }

                @Override
                public void onCancelled (@NonNull DatabaseError databaseError){

                }

        });





    }




//options menu on the toolbar
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            // All member functions
            case R.id.shareGroupLabel:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.joinGroup));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.shareLinkMember, groupID, getSupportActionBar().getTitle(), groupID));
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_app_via)));
                return true;
            case R.id.membersLabel:
                Intent toMembersItem = new Intent(getApplicationContext(), GroupMembersActivity.class);
                toMembersItem.putExtra("groupID", groupID);
                startActivity(toMembersItem);
                return true;
            case R.id.historicalGroupLabel:
                Intent toHistoricalGroupItem=new Intent(getApplicationContext(), GroupLogsActivity.class);
                toHistoricalGroupItem.putExtra("groupID", groupID);
                startActivity(toHistoricalGroupItem);
                return true;
            // Only admin functions
            case 3: // Edit group
                Intent editintent=new Intent(getApplicationContext(),EditGroupActivity.class);
                editintent.putExtra("groupID", groupID);
                startActivity(editintent);
                return true;
            case 4: // Delete group
                new AlertDialog.Builder(GroupActivity.this)
                        .setIcon(R.drawable.ic_remove_circle_outline)
                        .setTitle(R.string.removingGroupTitle)
                        .setMessage(R.string.removingGroupMsg)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                removeGroup(groupID);
                                Intent intent=new Intent(getApplicationContext(), HomeActivity.class);
                                startActivity(intent);

                            }
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .show();
                return true;
             // Only member functions
            case 5: // Leave group
                new AlertDialog.Builder(GroupActivity.this)
                        .setIcon(R.drawable.ic_flag)
                        .setTitle(R.string.leavingGroupTitle)
                        .setMessage(R.string.leavingGroupMsg)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (leaveGroup(currentUser.getUid()) == true){
                                    Toast.makeText(getApplicationContext(), R.string.notInGroup, Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



//set image for each category chosen for the group
    private void setImage(String category){
        if (category.equals(getString(R.string.DefaultCategoryLabel))) {
            sw.setImageResource(R.drawable.category_travel);
        } else if (category.equals(getString(R.string.CoupleLabel))) {
            sw.setImageResource(R.drawable.category_couple);
        } else if (category.equals(getString(R.string.SharedHouseLabel))) {
            sw.setImageResource(R.drawable.category_house_shared);
        } else if (category.equals(getString(R.string.EventLabel))) {
            sw.setImageResource(R.drawable.category_party);
        } else if (category.equals(getString(R.string.ProjectLabel))) {
            sw.setImageResource(R.drawable.category_project);
        } else if (category.equals(getString(R.string.otherLabel))) {
            sw.setImageResource(R.drawable.category_other);
        }
    }


//option for members
    private boolean leaveGroup(String memberID){
        final DatabaseReference dbMemberRef = databaseReference.child(DBNodes.MEMBERS);
        dbMemberRef.child(memberID).removeValue();
        return true;
    }
//option for admin
    private void removeGroup(String groupID) {
        Toast.makeText(getApplicationContext(), R.string.groupRemoved, Toast.LENGTH_SHORT).show();
        final DatabaseReference dbGroupRef = firebaseDatabase.getInstance().getReference(DBNodes.GROUPS);
        dbGroupRef.child(groupID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                finish();
            }
        });

    }






}
