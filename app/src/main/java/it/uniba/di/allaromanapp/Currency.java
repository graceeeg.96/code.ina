package it.uniba.di.allaromanapp;

public class Currency{

    private String code;
    private String currency;


    public Currency(String code, String currency){
        this.code=code;
        this.currency=currency;
    }
    public String getCode() {
        return code;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
