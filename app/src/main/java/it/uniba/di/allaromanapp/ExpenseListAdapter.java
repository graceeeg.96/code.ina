package it.uniba.di.allaromanapp;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

public class ExpenseListAdapter extends ArrayAdapter<Expense> {

    private Context mContext;
    int mResource;
    String currencyGroupCode;

    public ExpenseListAdapter(Context context, int resource, ArrayList<Expense> objects, String currencyGroupCode){
        super(context, resource, objects);
        mContext=context;
        mResource=resource;
        this.currencyGroupCode = currencyGroupCode;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String expenseTitle = getItem(position).getTitle();
        Float expenseAmount = getItem(position).getAmount();
        String payedBy = getItem(position).getPayedBy();
        String time = getItem(position).getTime();
        String date = getItem(position).getDate();
        ColorGenerator generator = ColorGenerator.MATERIAL;
        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(expenseTitle.charAt(0)).toUpperCase(), generator.getColor(position));
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        ImageView expenseIcon = convertView.findViewById(R.id.expense_icon);
        TextView expenseTitleLabel = convertView.findViewById(R.id.expense_title);
        TextView amountLabel = convertView.findViewById(R.id.amount_name);
        TextView payedByLabel = convertView.findViewById(R.id.Payedby_Label);
        TextView dateTimeLabel = convertView.findViewById(R.id.DateTime_Label);

        amountLabel.setText(String.format("%.2f",expenseAmount) + " " + currencyGroupCode);
        payedByLabel.setText(Html.fromHtml(getContext().getString(R.string.PayedbyUsernameLabel, payedBy)));
        dateTimeLabel.setText(date+ " "+ getContext().getResources().getString(R.string.at_time) + " "+ time);
        expenseTitleLabel.setText(expenseTitle);
        expenseIcon.setImageDrawable(drawable);
        return convertView;
    }
}
