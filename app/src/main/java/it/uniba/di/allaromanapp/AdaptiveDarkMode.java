package it.uniba.di.allaromanapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;


public class AdaptiveDarkMode{

    private SensorManager sensorManager = null;
    private float sensorValue = -1;

    public AdaptiveDarkMode(final Context context){
        final SharedPreferences pref = context.getApplicationContext().getSharedPreferences("AllaRomanAppSettings", 0);
        final SharedPreferences.Editor prefEditor = pref.edit();

        final AlertDialog message = new AlertDialog.Builder(context)
                .setIcon(R.drawable.settings_brightness)
                .setTitle(R.string.adaptiveDarkModeTitle)
                .setMessage(R.string.adaptiveDarkModeMsg)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                        prefEditor.putBoolean("adaptiveDarkMode", false);
                        prefEditor.putBoolean("darkMode", true);
                        prefEditor.apply();
                    }
                })
                .setNegativeButton(R.string.dontShowAgain, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        prefEditor.putBoolean("adaptiveDarkMode", false);
                        prefEditor.apply();
                    }
                }).create();




        final boolean adaptiveCurrentDarkMode = pref.getBoolean("adaptiveDarkMode", true);
        this.sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor light = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (adaptiveCurrentDarkMode){ // Prevention to reduce battery drain
            this.sensorManager.registerListener(new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent event) {
                    if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                        if (event.values[0] < 3 || event.values[0] > 15) {
                            sensorValue = event.values[0];
                        }
                        if (sensorValue == 0) {
                            if (adaptiveCurrentDarkMode && !message.isShowing()){
                                message.show();
                                sensorManager.unregisterListener(this);  // Reduce battery drain
                            }
                            Log.i("luce", String.valueOf(sensorValue));
                        } else {
                            Log.i("luce", String.valueOf(sensorValue));
                        }
                    }
                }



                @Override
                public void onAccuracyChanged(Sensor sensor, int i) {

                }
            }, light, SensorManager.SENSOR_DELAY_UI);
        }





    }
}


